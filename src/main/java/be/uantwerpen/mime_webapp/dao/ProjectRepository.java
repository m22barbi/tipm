package be.uantwerpen.mime_webapp.dao;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;

import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;
import be.uantwerpen.mime_webapp.model.Project;
import static java.util.Comparator.*;


public class ProjectRepository {

	private static File dataFile = new File(Settings.DATA_FILE); //no caching for know
	private static List<Project> projects = null;

	private ProjectRepository() {}

	public static Project findByName(String name){
		load();
		for(Project proj: projects){
			if(proj.getName().equalsIgnoreCase(name))
				return proj;
		}
		return null;
	}

	public static Project saveProject(Project projectEntity){
		System.out.println("dao>>saveProject(" + projectEntity.getName() + ")");
		load();
		projects.remove(projectEntity); //why? remove old 'version' using equals (based on name0
		for(FileItem item: projectEntity.getFileItems())
			item.getId();
		projects.add(projectEntity);
		save();
		return projectEntity;
	}

	public static void removeProject(String name){
		projects.remove(new Project(name));
		save();
	}

	public static List<Project> findAll(){
		load();
		return projects;
	}

	public static FileItem findItemById(String itemId){
		load();
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				System.out.println(item.getId());
				if(item.getId().equals(itemId))
					return item;
			}
		}
		return null;
	}

	public static Integer findNextVersion(String logicalName){
		load();
		int maxVersion = 0;
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				if(item.getLogicalName().equals(logicalName)) {
					maxVersion = Math.max(item.getVersion(),maxVersion);
				}

			}
		}
		return maxVersion+1;
	}

	public static FileItem removeItem(String itemId) {
		load();
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				if(item.getId().equals(itemId)){
					project.remove(item);
					save();
					return item;
				}
			}
		}
		return null;
	}

	//xml stuff to save/load
	@SuppressWarnings("unchecked")
	private static void load(){
		//no need to reload everytime.
		if(projects != null) return;
		if(!dataFile.exists())
		{
			projects = new ArrayList<>();
			return;
		}
		try{
			XStream xstream = getSerializer();
			FileReader reader = new FileReader(dataFile);
			projects = (List<Project>) xstream.fromXML(reader);
			reader.close();
			//inverse projects, so last added project is first
			Collections.reverse(projects);
			//Group by dataset name, within each project
			for(Project proj: projects) {
				List<FileItem> fileItems = proj.getFileItems();
				//see https://stackoverflow.com/questions/30382453/java-stream-sort-2-variables-ascending-desending
				fileItems = fileItems.stream().sorted(comparing(FileItem::getLogicalName)
										.thenComparing(comparing(FileItem::getVersion).reversed()))
										.collect(Collectors.toList());
				proj.setFileItemsItems(new ArrayList<FileItem>(fileItems));
			}
			System.out.println("dao>>Loading " + dataFile.getName());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void save(){
		XStream xstream = getSerializer();
		try {
			if(!dataFile.exists()) {
				dataFile.getParentFile().mkdirs();
			}
			FileWriter writer = new FileWriter(dataFile);
			xstream.toXML(projects, writer);
			writer.close();
			System.out.println("Saving " + dataFile.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static XStream getSerializer() {
		XStream xstream = new XStream();
		xstream.alias("project", Project.class);
		xstream.alias("item", FileItem.class);
		xstream.alias("patterns", PatternSet.class);
		xstream.addPermission(AnyTypePermission.ANY);
		return xstream;
	}

	public static Project findProjectByItem(FileItem currentItem) {
		load();
		for(Project project: projects){
			for(FileItem item: project.getFileItems()){
				System.out.println(item.hashCode() + "    "  + currentItem.hashCode());
				// == same instance we don't want a similar object we want the exact same.
				if(item == currentItem)
					return project;
			}
		}
		return null;
	}

	public static FileItem getLatestItem(Project project, String logicalName) {
		load();
		FileItem latest = null;
		for(FileItem item: project.getFileItems()) {
			if(item.getLogicalName().equals(logicalName) &&
				(latest == null || latest.getVersion() < item.getVersion())) {
				latest = item;
			}
		}
		return latest;
	}

}
