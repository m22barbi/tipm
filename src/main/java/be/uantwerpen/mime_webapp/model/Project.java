package be.uantwerpen.mime_webapp.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;


public class Project implements Serializable{

	private static final long serialVersionUID = 399579716649232694L;

    private String name;

    @JsonManagedReference("items")
    private ArrayList<FileItem> items = new ArrayList<>();

    public Project() {}

    public Project(String name) {
        this.name = name;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FileItem> getFileItems() {
		return items;
	}

	public void setFileItemsItems(List<FileItem> items) {
		this.items = new ArrayList<>(items);
	}

	public void add(FileItem item)
	{
		item.getId();
		this.items.add(item);
	}

	public void remove(FileItem item)
	{
		this.items.remove(item);
		File file = item.getFile();

		if(file.exists()){
			//we don't remove if the file is still used by other items
			for(FileItem fileitem : items) {
				if(fileitem.getFile().equals(file)) {
					return;
				}
			}

			file.delete();
			System.err.println("Deleting file " + file.getAbsolutePath());
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Project)) return false;
		return this.name.equals(((Project)obj).name);
	}

	@Override
    public String toString() {
        return String.format("Project[name='%s']", name);
    }



}
