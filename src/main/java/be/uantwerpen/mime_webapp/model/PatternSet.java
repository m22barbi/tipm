package be.uantwerpen.mime_webapp.model;

public class PatternSet {
	private String filename;
	private String filenameOccurrences;

	private String type; //e.g.
	private String columns;
	private String algorithm;
	private String support;

	private Long noPatterns;

	public String getLabel() {
		String[][] dict = new String[][]{
				{"Eclat", "itemsets"},
				{"Charm_bitset","closed itemsets"},
				{"Charm_MFI", "maximal itemsets"},
				{"AprioriRare", "rare itemsets"},
				{"PrefixSpan", "sequential patterns"},
				{"CM-ClaSP", "closed sequential patterns"},
				{"VMSP", "maximal sequential patterns"}};
		String label = algorithm + " " + type;
		for(int i=0; i<dict.length; i++) {
			if(algorithm.equals(dict[i][0])) {
				label = dict[i][1];
			}
		}
		label += " " + columns;
		return label;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilenameOccurrences() {
		return filenameOccurrences;
	}

	public void setFilenameOccurrences(String filenameOccurrences) {
		this.filenameOccurrences = filenameOccurrences;
	}

	public String getType() { //either "itemsets" or "sequential patterns"
		return type;
	}

	public void setType(String type) {
		if(type.equals("itemsets") || type.equals("sequential patterns")) {
			this.type = type;
		}
		else {
			throw new RuntimeException("Unknown type: " + type);
		}

	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getSupport() {
		return support;
	}

	public void setSupport(String support) {
		this.support = support;
	}

	public Long getNoPatterns() {
		return noPatterns;
	}

	public void setNoPatterns(Long noPatterns) {
		this.noPatterns = noPatterns;
	}

	@Override
    public String toString() {
        return String.format(
                "PatternSet[filename='%s']",  getFilename());
    }
}
