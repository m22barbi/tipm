package be.uantwerpen.mime_webapp.model;

import java.io.Serializable;

public class MySession implements Serializable {
	private static final long serialVersionUID = 1L;

	private String currentProject;
	private String currentItem;

	public MySession(){}

	public MySession(String currentProject, String currentItem) {
		super();
		this.currentProject = currentProject;
		this.currentItem = currentItem;
	}

	public String getCurrentProject() {
		return currentProject;
	}
	public void setCurrentProject(String currentProject) {
		this.currentProject = currentProject;
	}
	public String getCurrentItem() {
		return currentItem;
	}
	public void setCurrentItem(String currentData) {
		this.currentItem = currentData;
	}

}
