package be.uantwerpen.mime_webapp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.ldataminining.preprocessing.ArffStatistics;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.FindNullPattern;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.mime_webapp.model.FileItem;

@RestController
public class FileItemMetadataController extends AbstractController {



	/**
	 * returns name/type for all attributes
	 *
	 * @param request
	 * @return
	 */
	@GetMapping(value="/rest/metadata/attributes")
	public @ResponseBody List<Map<String,String>> getSchema(@RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);

		if(item == null)
			return new ArrayList<>();

		if(!item.isArff()) {
			System.err.println("Only arff supported");
			return new ArrayList<>();
		}
		try {
			List<String> types = ArffUtils.getAttributeValues(item.getFile());
			List<String> names =  ArffUtils.getAttributeNames(item.getFile());
			List<Map<String,String>> metadata = new ArrayList<>();
			for(int i=0; i<names.size(); i++){
				metadata.add(CollectionUtils.asMap(new String[][]{{names.get(i), types.get(i)}}));
			}
			return metadata;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping(value="/rest/metadata/attribute-and-statistics")
	public @ResponseBody List<Map<String,String>> getAttributeStatistics(@RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		FileItem currentItem = getCurrentItem(request, id);

		if(currentItem == null) {
			return null;
		}

		if(!currentItem.isArff())
			return null;

		try {
			return ArffStatistics.getAllAttributeStatisticsFast(currentItem.getFile());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error computing statistics: " + e.getMessage());
		}
	}


	@GetMapping(value="/rest/metadata/nullpatterns")
	public @ResponseBody List<List<String>> getNullPatterns(@RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);

		if(item == null)
			return null;

		if(!item.isArff()) {
			System.err.println("Only arff supported");
			return null;
		}
		try {
			return FindNullPattern.findNullPattern(item.getFile(),10);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GetMapping(value="/rest/metadata/fileitem")
	public @ResponseBody FileItem getFileItem(@RequestParam("id") Optional<String> id, HttpServletRequest request)
	{
		FileItem item = getCurrentItem(request, id);
		if(item == null)
			return null;
		else
			return item;
	}

}
