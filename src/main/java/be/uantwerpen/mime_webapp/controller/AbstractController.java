package be.uantwerpen.mime_webapp.controller;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.MySession;

@RestController
public abstract class AbstractController {

	protected FileItem getCurrentItem(HttpServletRequest request, Optional<String> id) {
		FileItem currentInput = null;
		if(id.isPresent()) {
			String parsedId = URLDecoder.decode(id.get(), StandardCharsets.UTF_8);
			currentInput = ProjectRepository.findItemById(parsedId.trim());
			if(currentInput != null) return currentInput;
		}

		MySession session = (MySession) request.getSession().getAttribute("mySession");
		if(session != null)
			return ProjectRepository.findItemById(session.getCurrentItem());
		else throw new IllegalStateException("not invalid passed or no session established");
	}
}
