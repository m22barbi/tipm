//TODO: GET LEGAL RIGHT TO DISTRIBUTE THE .JAR

package be.uantwerpen.mime_webapp.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffDenseUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;
import hybrid.model.HybridSoftClustering;
import hybrid.model.ResultModel;

@RestController
public class MltClusteringController extends AbstractController {
	@PostMapping(value = "/image/mtlClustering")
	public synchronized ResponseEntity<?> mtlClustering(
		HttpServletRequest request,
		@RequestParam("id") Optional<String> fileItemId,
		@RequestParam("filename") String patternFilename,
		@RequestParam("canalid") int canalId
	) throws IOException {
		FileItem currentItem = getCurrentItem(request, fileItemId);
		Table table = ArffDenseUtils.loadArff(currentItem.getFile());
		final Optional<PatternSet> optPatternSet = currentItem.getPatterns().stream().filter(x -> x.getFilename().equals(patternFilename)).findFirst();
		if(!optPatternSet.isPresent()) {
			return ResponseEntity.badRequest().body("pattern not found");
		}
		PatternSet patternSet = optPatternSet.get();
		File patternOccFile = new File(Settings.FILE_FOLDER + patternSet.getFilenameOccurrences());
		Table occurrencesTable = CSVUtils.loadCSV(patternOccFile);
		File patternFile = new File(Settings.FILE_FOLDER + patternSet.getFilename());
		Table patternTable = CSVUtils.loadCSV(patternFile);

		File file = generateDatasetFile(table, canalId);
		File clustering = generateOccurrencesFile(occurrencesTable, patternTable, canalId);

		String[] args = {
			"-d", file.getAbsolutePath(),
			"-c", clustering.getAbsolutePath()
		};


		ResultModel[] results = HybridSoftClustering.runAsApi(args);

		if(results.length == 0) return ResponseEntity.ok("No solution found");

		createNewDatasetFromResultModel(occurrencesTable, results[0]);

		//TODO: re encode using those values

		return ResponseEntity.ok(null);
	}



	private void createNewDatasetFromResultModel(Table occurrencesTable, ResultModel resultModel) {
		List<Integer> patternIds = Arrays.stream(resultModel.clustering).boxed().toList();
		//tree map so the output is sorted.
		Map<Integer, Integer> windowValueMap = new TreeMap<>();
		for(List<String> row : occurrencesTable.getRowsStartingFrom1()) {
			int patternId = Integer.parseInt(row.get(1));
			if(patternIds.contains(patternId)) {
				windowValueMap.put(Integer.valueOf(row.get(0)), patternId);
			}
		}

	}



	private File generateOccurrencesFile(Table table, Table patternTable, int canalId) throws IOException {
		List<List<String>> rows = table.getRowsStartingFrom1();

		Map<Integer, List<Integer>> windowPatterns = new HashMap<>();
		Map<Integer, String[]> patternValue = new HashMap<>();

		ListIterator<List<String>> it = patternTable.getRowsStartingFrom1().listIterator();
		while(it.hasNext()) {
			int patternId = it.nextIndex() + 1;
			String pattern = it.next().get(0);

			//TODO: modify mlt-clustering to fix this
			//patterns that contains ; have conditions that are not taken into account by mlt-clustering so we skip them to be safe.
			if(pattern.contains(";")) continue;

			String[] labeledValues = pattern.split(" ");
			String[] rawValues = new String[labeledValues.length];
			for(int i = 0; i < labeledValues.length; i++) {
				String val = labeledValues[i];
				// the format is columnName=Value so since we only care about the value we split by equal filter the rest out.
				rawValues[i] = val.split("=")[1];
			}

			patternValue.put(patternId, rawValues);
		}

		for(List<String> row : rows) {
			Integer window = Integer.valueOf(row.get(0));
			Integer patternId = Integer.valueOf(row.get(1));

			windowPatterns.putIfAbsent(patternId, new LinkedList<>());
			windowPatterns.get(patternId).add(window);
		}

		File file = new File(Settings.TMP_CLUSTERING_FILE);
		FileWriter fw = new FileWriter(file);

		//TODO comment this properly
		for(Integer patternId : windowPatterns.keySet()) {
			String[] pattern = patternValue.get(patternId);
			if(pattern == null) continue;

			fw.append(String.join(" ", pattern) + "\n");
			fw.append(" " + CollectionUtils.join(windowPatterns.get(patternId), " ") + "\n");
		}

		fw.close();
		return file;
	}



	private File generateDatasetFile(Table table, int canalId) throws IOException {
		List<String> header = table.getRows().get(0);
		int windowIndex = header.indexOf("Window");
		String currentWindow = "";
		StringBuilder sb = new StringBuilder();
		List<List<String>> rows = table.getRowsStartingFrom1();
		for(List<String> row : rows) {
			String window = row.get(windowIndex);
			if(!currentWindow.equals(window)) {
				sb.append('\n');
				currentWindow = window;
			} else {
				sb.append(",");
			}
			sb.append(row.get(canalId));
		}

		File file = new File(Settings.TMP_CLUSTERING_INSTANCE_FILE);
		FileWriter fw = new FileWriter(file);
		fw.append(sb.toString().trim());
		fw.append("\n[EOF]");
		fw.close();
		return file;
	}
}
