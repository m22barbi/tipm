package be.uantwerpen.mime_webapp.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.dao.ProjectRepository;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.Project;

@Controller
public class FileItemUploadController extends AbstractController{
	@GetMapping(value="/rest/upload")
	public @ResponseBody String provideUploadInfo() {
		return "You can upload a file by posting to this same URL.";
	}

	@PostMapping(value="/rest/upload")
	public @ResponseBody String handleFileUpload(
			@RequestParam("project") String project,
			@RequestParam("name") String name,
			@RequestParam("apimode") Optional<Boolean> apiMode,
			@RequestParam("file") MultipartFile file){
		try {
			if (file.isEmpty())
				return "You failed to upload " + name + " because the file was empty.";

			//ensuring the file format is preserved
			String format = getFileExtension(file.getOriginalFilename());
			if(!name.endsWith(format)) {
				name += '.'+format;
			}

			byte[] bytes = file.getBytes();
			if(!new File(Settings.FILE_FOLDER).exists()) {
				new File(Settings.FILE_FOLDER).mkdirs();
			}
			File output = new File(Settings.FILE_FOLDER + name);
			System.out.println(output.getAbsolutePath());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(output));
			stream.write(bytes);
			stream.close();
			//also create metadata in database
			Project projectEntity = ProjectRepository.findByName(project);
			if(projectEntity == null)
				throw new RuntimeException("Expected project");
			FileItem fileItem = new FileItem();
			fileItem.setFilename(name);
			fileItem.setLogicalName(IOUtils.getFilenameNoExtension(output));
			if(name.toUpperCase().endsWith("CSV")){
				long noRows = IOUtils.countLines(output) - 1;
				long noColumns = CSVUtils.getNoColumns(output);
				fileItem.setNoRows(noRows);
				fileItem.setNoColumns(noColumns);
			}
			else if(name.toUpperCase().endsWith("ARFF")){
				long noRows = ArffUtils.getNumberOfRows(output);
				long noColumns = ArffUtils.getAttributeNames(output).size();
				fileItem.setNoRows(noRows);
				fileItem.setNoColumns(noColumns);
			}
			fileItem.getStackOperations().add("uploaded " + name);
			projectEntity.add(fileItem);
			ProjectRepository.saveProject(projectEntity);
			System.out.println("Adding item");

			// i added this for TIPM-UI my fork of TIPM this allows to have a much simpler interaction with the server
			// without breaking compatibility with the original TIPM interface
			if(apiMode.isPresent() && apiMode.get()) {
				return fileItem.getId();
			}
			return "<html><head><meta http-equiv=\"refresh\" content=\"1;url=/list\"/></head><body>You successfully uploaded " + name + "!</body></html>";
		}
		catch (Exception e) {
			return "You failed to upload " + name + " => " + e.getMessage();
		}
	}

	private String getFileExtension(String name) {
		System.out.println(name);
		final String[] fileparts = name.split("\\.");
		return fileparts[fileparts.length - 1];
	}

}
