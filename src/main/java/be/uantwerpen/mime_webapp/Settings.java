package be.uantwerpen.mime_webapp;

public class Settings {
	//we mask the constructor to protect from any useless instances
	private Settings(){}
	// -1 == disabled other values are in seconds
	public static final long SMPF_TIMEOUT = -1;
	public static final long MAX_UPLOAD_FILE_SIZE_KB = 2200800;
	public static final String SMPF_MAX_RAM_ALLOCATION = "2G";
	public static final String FILE_FOLDER = "./data/upload/";
	public static final String DATA_FILE = "./data/projects.xml";
	public static final String SPMF_JAR = "./lib/spmf.jar";
	public static final String PBAD = "../pbad-public/";
	public static final String PYTHON3 = "/bin/python3";
	//this variable control where the uploaded images are stored and their's .dat files
	//if you don't want the images to be stored in the tmp directory, you can change this
	//THIS PATH MUST END WITH /
	public static final String IMAGE_FOLDER = System.getProperty("java.io.tmpdir") + "/tipm/";
	//this variable control where the cached images go.
	//cached images are images generated when you try to convert a fileitem(CSV/ARFF) to an image.
	//THIS PATH MUST END WITH /
    public static final String IMAGE_CACHE = System.getProperty("java.io.tmpdir") + "/tipmCache/";
    public static final String TMP_CLUSTERING_FILE = System.getProperty("java.io.tmpdir") + "/tipm_cluster_cache.txt";
	public static final String TMP_CLUSTERING_INSTANCE_FILE = System.getProperty("java.io.tmpdir") + "/tipm_cluster_instance.txt";
}
