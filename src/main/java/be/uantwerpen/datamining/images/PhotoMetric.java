package be.uantwerpen.datamining.images;

/**
 * @author Marc barbier
 * based on
 * https://www.awaresystems.be/imaging/tiff/tifftags/photometricinterpretation.html
 */
public class PhotoMetric {
    private PhotoMetric() {}
    public static final int PHOTOMETRIC_MINISWHITE = 0;
    public static final int PHOTOMETRIC_MINISBLACK = 1;
    public static final int PHOTOMETRIC_RGB = 2;
    public static final int PHOTOMETRIC_PALETTE = 3;
    public static final int PHOTOMETRIC_MASK = 4;
    public static final int PHOTOMETRIC_SEPARATED = 5;
    public static final int PHOTOMETRIC_YCBCR = 6;
    public static final int PHOTOMETRIC_CIELAB = 8;
    public static final int PHOTOMETRIC_ICCLAB = 9;
    public static final int PHOTOMETRIC_ITULAB = 10;
    public static final int PHOTOMETRIC_LOGL = 32844;
    public static final int PHOTOMETRIC_LOGLUV = 32845;

    public static String photometricInterpretationToString(int id) {
		switch(id) {
			case PHOTOMETRIC_MINISWHITE: return "PHOTOMETRIC_MINISWHITE";
			case PHOTOMETRIC_MINISBLACK: return "PHOTOMETRIC_MINISBLACK";
			case PHOTOMETRIC_RGB: return "PHOTOMETRIC_RGB";
			case PHOTOMETRIC_PALETTE: return "PHOTOMETRIC_PALETTE";
			case PHOTOMETRIC_MASK: return "PHOTOMETRIC_MASK";
			case PHOTOMETRIC_SEPARATED: return "PHOTOMETRIC_SEPARATED";
			case PHOTOMETRIC_YCBCR: return "PHOTOMETRIC_YCBCR";
			case PHOTOMETRIC_CIELAB: return "PHOTOMETRIC_CIELAB";
			case PHOTOMETRIC_ICCLAB: return "PHOTOMETRIC_ICCLAB";
			case PHOTOMETRIC_ITULAB: return "PHOTOMETRIC_ITULAB";
			case PHOTOMETRIC_LOGL: return "PHOTOMETRIC_LOGL";
			case PHOTOMETRIC_LOGLUV: return "PHOTOMETRIC_LOGLUV";
			default: return "Unknown";
		}
	}
}
