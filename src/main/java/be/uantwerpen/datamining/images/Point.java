package be.uantwerpen.datamining.images;

public class Point extends Vector {
    public Point(int x, int y, double value, Integer centroid) {
        super(x, y);
        this.value = value;
        this.centroid = centroid;
    }

    public Point(int x, int y, double value) {
        this(x, y, value, null);
    }

    public double value;
    public Integer centroid;
}
