package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.tomcat.util.buf.StringUtils;

import be.uantwerpen.exception.InvalidHeapSizeException;
import be.uantwerpen.exception.SPMFException;
import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.utils.AlgorithmParameters;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.CommandLineUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Triple;
import be.uantwerpen.mime_webapp.Settings;
import ca.pfv.spmf.algorithmmanager.AlgorithmManager;
import ca.pfv.spmf.algorithmmanager.DescriptionOfAlgorithm;
import ca.pfv.spmf.algorithmmanager.DescriptionOfParameter;

public final class MineUsingSPMF {
	private MineUsingSPMF(){}

	/**
	 * @param isSequential
	 * @param arff
	 * @param columns
	 * @param algorithm
	 * @param parametersMap
	 * @return Pair < Pattern File , Ocurences >
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static Pair<File, File> runMining(boolean isSequential, File arff, List<String> columns, String algorithm, Map<String, String> parametersMap) throws IOException, InterruptedException {
		Triple<File, File, Map<String,Integer>> dictFileTransFilePair = ArffToSPMF.arffToSPMF(isSequential, arff, columns);
		String [] processedParameters = processParameters(algorithm, parametersMap, dictFileTransFilePair.getThird());

		File log = new File("./temp/pattern_mining_" + arff.getName() + ".log");
		File outputRaw = new File("./temp/" + algorithm + "_out_raw_" + arff.getName() + ".txt");

		//java + MEMORY_ALLOCATION + file input + file output
		String[] commandBase = new String[]{"java", "-Xmx" + Settings.SMPF_MAX_RAM_ALLOCATION, "-jar", new File(Settings.SPMF_JAR).getAbsolutePath(), "run", algorithm,
				dictFileTransFilePair.getSecond().getAbsolutePath(),
				outputRaw.getAbsolutePath() };

		// Add parameters to the command minsup HAVE to be included
		//the last parameter gives the occurences
		String[] command = ArrayUtils.addAll(commandBase, processedParameters);

		CommandLineUtils.runCommandInUserDir(command, log, Settings.SMPF_TIMEOUT);

		IOUtils.printHead(log, 100);
		String logStr = IOUtils.readFileFlat(log);
		if(logStr.contains("Exception")) {
			throw new SPMFException("Exception during mining with SPMF.\n" + logStr);
		}

		if(logStr.contains("Invalid maximum heap size: -Xmx")) {
			throw new InvalidHeapSizeException("Invalid maximum heap size" + Settings.SMPF_MAX_RAM_ALLOCATION);
		}

		File outputOccurrences = new File(Settings.FILE_FOLDER, arff.getName() + "_" + algorithm + "_" + CollectionUtils.join(columns,"_") + "_" + StringUtils.join(parametersMap.values(), '_') + "_patterns_occurrences.csv");
		File outputReadable = new File(Settings.FILE_FOLDER, arff.getName() + "_" + algorithm + "_" + CollectionUtils.join(columns,"_") + "_" + StringUtils.join(parametersMap.values(), '_') + "_patterns.csv");
		if(isSequential)
			ArffToSPMF.saveOutputReadableSequential(dictFileTransFilePair.getFirst(), outputRaw, outputReadable, outputOccurrences);
		else
			ArffToSPMF.saveOutputReadableItemset(dictFileTransFilePair.getFirst(), outputRaw, outputReadable, outputOccurrences);

		if(outputReadable.exists() && !IOUtils.isEmptyFile(outputReadable)) {
			dictFileTransFilePair.getFirst().delete();
			dictFileTransFilePair.getSecond().delete();
			log.delete();
			outputRaw.delete();
		}
		return new Pair<>(outputReadable, outputOccurrences);
	}

	private static void preProcessParameters(Map<String, String> parametersMap, Map<String, Integer> idMap) {
		if(parametersMap.containsKey(AlgorithmParameters.REQUIRED_ITEMS)) {
			String requiredItems = parametersMap.get(AlgorithmParameters.REQUIRED_ITEMS);
			String[] items = requiredItems.split(",");
			List<String> itemsList = new ArrayList<>();
			for(String item : items) {
				if(idMap.containsKey(item)) {
					itemsList.add(idMap.get(item).toString());
				}
			}
			parametersMap.put(AlgorithmParameters.REQUIRED_ITEMS, CollectionUtils.join(itemsList, ","));
		}
	}

	private static String[] processParameters(String algorithm, Map<String, String> parametersMap, Map<String, Integer> map) throws IOException {
		preProcessParameters(parametersMap, map);
		DescriptionOfAlgorithm description = null;
		try {
			description = AlgorithmManager.getInstance().getDescriptionOfAlgorithm(algorithm);
		} catch (Exception e) {
			throw new RuntimeException("could not load SPMF's AlgorithmManager ", e);
		}
		if(description == null) {
			throw new InvalidParameterException("Algorithm " + algorithm + " not found");
		}

		DescriptionOfParameter[] parametersDescriptions = description.getParametersDescription();
		String[] processedParameters = new String[parametersDescriptions.length];
		int parameterCount = 0;
		int i = 0;
		for(DescriptionOfParameter param : parametersDescriptions) {
			//we want to have the ids regardless of the algorithm
			if(param.name.equals("Show sequence ids?") || param.name.equals("Show transaction ids?")) {
				processedParameters[i] = "true";
				i++;
				//if the parameter was specified
				if(parametersMap.get(param.name) != null) {
					parameterCount++;
				}
				continue;
			}

			String value = parametersMap.get(param.name);
			if(value != null) {
				processedParameters[i] = value;
				parameterCount++;
			} else {
				processedParameters[i] = "";
			}
			i++;
		}

		if(parameterCount != parametersMap.size()) throw new InvalidParameterException("Invalid parameters for algorithm " + algorithm);

		return processedParameters;
	}
}
