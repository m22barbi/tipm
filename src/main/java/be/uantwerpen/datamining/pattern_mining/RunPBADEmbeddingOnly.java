package be.uantwerpen.datamining.pattern_mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import be.uantwerpen.ldataminining.model.Table;
import be.uantwerpen.ldataminining.preprocessing.ArffUtils;
import be.uantwerpen.ldataminining.preprocessing.CSVUtils;
import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.CommandLineUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.mime_webapp.Settings;
import be.uantwerpen.mime_webapp.model.FileItem;
import be.uantwerpen.mime_webapp.model.PatternSet;

/**
 * Run Python program in PBAD
 * See https://bitbucket.org/len_feremans/pbad/
 */
public class RunPBADEmbeddingOnly {

	String usagePBADInstall = "To install PBAD, take the following steps:\n" +
			">>git clone git@bitbucket.org:len_feremans/pbad.git\n" +
			">>cd pbad_pub\n" +
			">>cd src/utils/cython_utils/\n" +
			">>python3 setup.py build_ext --inplace\n" +
			"Remark that PBAD required python3, and numpy==1.16.3, pandas==0.24.2, scikit-learn==0.20.3, Cython==0.29.7 and scipy==1.2.1\n";


	public String computePBADScore(FileItem item, File scoreFile) throws IOException, InterruptedException {
		//convert arff2csv
		File arff = item.getFile();
		List<String> cols = ArffUtils.getAttributeNames(arff);
		List<List<String>> rows = ArffUtils.loadDataMatrix(arff);
		rows.add(0, cols);
		Table table = new Table();
		table.setRows(rows);
		File csv = new File(item.getFile().getAbsolutePath() + ".csv");
		CSVUtils.saveTable(table, csv);
		//run main_TIMP.py in PBAD
		//validation installation:
		File PBAD = new File(Settings.PBAD + "./src/main_TIPM.py").getAbsoluteFile();
		if(!new File(Settings.PYTHON3).exists()) {
			String msg = String.format("PBAD depends on Python3. Please download and install Python3.\n" +
					"Location of Python3 is configured in be.uantwerpen.mime_webapp.Settings.\n" +
					"Expected location of Python3 is: %s", new File(Settings.PYTHON3).getAbsolutePath());
			System.err.println(msg);
			throw new RuntimeException(msg);
		}
		if(!PBAD.exists()) {
			String msg = String.format("PBAD not found. Please download and install PBAD.\n" +
						"Location of PBAD is configured in be.uantwerpen.mime_webapp.Settings.\n" +
						"Expected location PBAD is: %s", PBAD);
			System.err.println(msg);
			System.err.println(usagePBADInstall);
			throw new RuntimeException(msg);
		}
		File log = new File("./temp/PBAD_log.txt");
		int status = CommandLineUtils.runCommandInUserDir(new String[]{Settings.PYTHON3, PBAD.getAbsolutePath(), "-?"}, log, 5);
		if(status != 0) {
			String logStr = IOUtils.readFileFlat(log);
			System.err.println("Error running PBAD with python3. Log:");
			System.err.println(logStr);
			System.err.println(usagePBADInstall);
			throw new RuntimeException("Error running PBAD");
		}

		String type = "all";
		List<String> itemsetFilenames = item.getPatterns().stream()
				.filter((p) -> p.getType().equals("itemsets"))
				.map((p) -> new File(Settings.FILE_FOLDER + p.getFilename()).getAbsolutePath())
				.collect(Collectors.toList());

		List<String> sequentialPatternFilenames = item.getPatterns().stream()
				.filter((p) -> p.getType().equals("sequential patterns"))
				.map((p) -> new File(Settings.FILE_FOLDER + p.getFilename()).getAbsolutePath())
				.collect(Collectors.toList());

		boolean onlyItemsets = !item.getPatterns().isEmpty() && itemsetFilenames.size() == item.getPatterns().size();
		boolean onlySequentialPatterns = !item.getPatterns().isEmpty()  && sequentialPatternFilenames.size() == item.getPatterns().size();

		if(onlyItemsets) {
			type = "itemset";
		}
		if(onlySequentialPatterns)
			type = "sequential";
		Set<String> columns = new TreeSet<>();
		for(PatternSet pattern: item.getPatterns()) {
			for(String col: pattern.getColumns().split(",")) { //can not be more than one actually
				columns.add(col);
			}
		}
		List<String> columnsSorted = new ArrayList<>(columns);
		String[] command = new String[]{Settings.PYTHON3, PBAD.getAbsolutePath(),
				"-input", csv.getAbsolutePath(),
				"-type", type,
				"-columns", CollectionUtils.join(columnsSorted,","),
				"-score_fname", scoreFile.getAbsolutePath()
		};
		if(type.equals("all") || type.equals("itemset")) {
			command = CollectionUtils.concatenate(command,
					new String[] {"-itemset_fnames", CollectionUtils.join(itemsetFilenames,",")});
		}
		if(type.equals("all") || type.equals("sequential")) {
			command = CollectionUtils.concatenate(command,
					new String[] {"-sequential_fnames", CollectionUtils.join(sequentialPatternFilenames,",")});
		}
		status = CommandLineUtils.runCommandInUserDir(command, log, 10);
		if(status != 0) {
			String logStr = IOUtils.readFileFlat(log);


			if(logStr.contains("Expecting column label")) {
				throw new RuntimeException("The data is not labelled you need a column named 'label'");
			}

			System.err.println("Error running PBAD. Log:");
			System.err.println(logStr);
			System.err.println(usagePBADInstall);
			throw new RuntimeException("Error running PBAD: " + logStr);
		}
		else {
				List<String> logStr = IOUtils.readFile(log);
				System.out.println(CollectionUtils.join(logStr));
				//last two lines contain AUX and AP, return
				return logStr.get(logStr.size()-3) + " " + logStr.get(logStr.size()-2);
		}
	}
}
