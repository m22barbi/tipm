package be.uantwerpen.datamining.pattern_mining;

import java.util.List;

public interface IRowPredicte {
	/**
	 * @param row the fill row
	 * @param index the index of the row
	 * @return true to keep the row
	 */
	boolean predicate(List<String> row, int index);
}
