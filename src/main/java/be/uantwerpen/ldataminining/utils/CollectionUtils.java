package be.uantwerpen.ldataminining.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.ldataminining.model.NaturalOrderComparator;
import be.uantwerpen.ldataminining.model.Pair;

import java.util.Set;
import java.util.TreeMap;

/**
 * Generic utilities for Collections and Arrays
 *
 * @author lfereman
 *
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class CollectionUtils {
	//Array stuff
	public static <T> T[] subarray(T[] A, int start, int end) {
		T[] C = (T[]) Array.newInstance(A.getClass().getComponentType(), end-start);
		System.arraycopy(A, start, C, 0, end-start);
		return C;
	}

	public static <T> T[] concatenate (T[] A, T[] B) {
		int aLen = A.length;
		int bLen = B.length;

		T[] result = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen+bLen);
		for(int i=0; i<aLen; i++)
			result[i] = A[i];
		for(int j=0; j<bLen; j++)
			result[j+ aLen] = B[j];
		return result;
	}

	public static <T> List<T> concatenate (List<T> A, List<T> B) {
		List<T> result = new ArrayList<T>(A);
		result.addAll(B);
		return result;
	}

	public static double[] concatenate (double[] A, double[] B) {
		double[] C = new double[A.length + B.length];
		System.arraycopy(A, 0, C, 0, A.length);
		System.arraycopy(B, 0, C, A.length, B.length);
		return C;
	}

	public static <T> T[] concatenate (T[]... arrays) {
		int len = 0;
		for(T[] arr: arrays){
			len += arr.length;
		}
		T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), len);
		int i=0;
		for(T[] arr: arrays){
			for(T el: arr){
				result[i++] = el;
			}
		}
		return result;
	}

	public static <T> boolean contains(final T[] array, final T v) {
		for (final T e : array)
			if (e == v || (v != null && v.equals(e)))
				return true;

		return false;
	}

	public static  boolean stringContainsAny(final String v, final String[] array) {
		for (final String e : array)
			if (v.contains(e))
				return true;
		return false;
	}

	public static <T> int indexOf(T c , T[] array ) {
		for( int i = 0 ; i < array.length ; i++ ) {
			if( array[i].equals(c) ) {
				return i;
			}
		}
		return -1;
	}

	public static Pair findByKey( List<Pair> pairs, String key)
	{
		for(Pair p: pairs)
			if(p.getFirst().equals(key))
				return p;
		return null;
	}

	/**
	 * For easy syntax, e.g. Map m = asMap(new String[]{{"k", 10}, {"x", "undefined"}})
	 * @param args
	 * @return
	 */
	public static Map<String,String> asMap(String[][] args)
	{
		Map<String, String> map = new TreeMap<>();
		for(int i=0; i< args.length; i++)
		{
			map.put(args[i][0], args[i][1]);
		}
		return map;
	}

	public static Map<String,String> asMap(Object... args)
	{
		Map<String, String> map = new TreeMap<>();
		for(int i=0; i< args.length; i+=2)
		{
			map.put(String.valueOf(args[i]), String.valueOf(args[i+1]));
		}
		return map;
	}

	public static <T>  Map<String,T> asMap2(Object... args)
	{
		Map<String, T> map = new TreeMap<>();
		for(int i=0; i< args.length; i+=2)
		{
			map.put(String.valueOf(args[i]), (T)(args[i+1]));
		}
		return map;
	}

	public static List<Map<String,String>> asListOfMaps(String[][] args)
	{
		List<Map<String,String>> maps = new ArrayList<>();
		for(String[] arr: args)
		{
			Map<String, String> map = new TreeMap<>();
			for(int i=0; i< args.length-2; i+=2){
				map.put(arr[i], arr[i+1]);
			}
			maps.add(map);
		}
		return maps;
	}

	public static <K extends Comparable>  List<K> sorted(Set<K> set) {
		List<K> sortedEntries = new ArrayList<K>(set);
		Collections.sort(sortedEntries, new NaturalOrderComparator());
		return sortedEntries;
	}

	public static <K,V extends Comparable<? super V>>  List<Entry<K, V>> entriesSortedByValues(Map<K,V> map) {
		List<Entry<K,V>> sortedEntries = new ArrayList<>(map.entrySet());
		Collections.sort(sortedEntries,  new Comparator<Entry<K,V>>() {
			@Override
			public int compare(Entry<K,V> e1, Entry<K,V> e2) {
				return e2.getValue().compareTo(e1.getValue());
			}
		}
				);
		return sortedEntries;
	}

	public static <K,V extends Comparable<? super V>>  List<Entry<K, V>> entriesSortedByValues(Map<K,V> map, final Comparator<V> comp) {
		List<Entry<K,V>> sortedEntries = new ArrayList<Entry<K,V>>(map.entrySet());
		Collections.sort(sortedEntries,  new Comparator<Entry<K,V>>() {
			@Override
			public int compare(Entry<K,V> e1, Entry<K,V> e2) {
				return comp.compare(e1.getValue(),e2.getValue());
			}
		}
				);
		return sortedEntries;
	}

	public static <K,V extends Comparable<? super V>>  List<Entry<K, V>> entriesSortedByValuesBigToSmall(Map<K,V> map) {
		List<Entry<K,V>> sortedEntries = new ArrayList<Entry<K,V>>(map.entrySet());
		Collections.sort(sortedEntries,  new Comparator<Entry<K,V>>() {
			@Override
			public int compare(Entry<K,V> e1, Entry<K,V> e2) {
				return e1.getValue().compareTo(e2.getValue());
			}
		}
				);
		return sortedEntries;
	}

	public static <K,V extends Collection<?>>  List<Entry<K, V>> entriesSortedByValueSize(Map<K,V> map) {
		List<Entry<K,V>> sortedEntries = new ArrayList<Entry<K,V>>(map.entrySet());
		Collections.sort(sortedEntries,  new Comparator<Entry<K,V>>() {
			@Override
			public int compare(Entry<K,V> e1, Entry<K,V> e2) {
				return e2.getValue().size() - e1.getValue().size();
			}
		}
				);
		return sortedEntries;
	}


	public static <T extends Collection<?>> String join(T collection, String seperator) {
		return join(collection, seperator, "%s");
	}

	public static <T extends Collection<?>> String join(T collection, String seperator, String formatter) {
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(Object el: collection)
		{
			buffer.append(String.format(Locale.ENGLISH, formatter,el));
			if(idx != collection.size() -1)
				buffer.append(seperator);
			idx++;
		}
		return buffer.toString();
	}


	public static <T extends Collection<?>> String join(T collection) {
		return join(collection, ", ");
	}


	public static <T> String join(T[] array) {
		return CollectionUtils.join(array,", ");
	}

	public static String join(int[] array) {
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(int el: array)
		{
			buffer.append(el);
			if(idx != array.length -1)
				buffer.append(",");
			idx++;
		}
		return buffer.toString();
	}

	public static String join(double[] array) {
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(double el: array)
		{
			buffer.append(el);
			if(idx != array.length -1)
				buffer.append(",");
			idx++;
		}
		return buffer.toString();
	}

	public static <K,V> String join(Map<K,V> collection) {
		return join(collection, ":", ", ");
	}

	public static <K,V> String join(Map<K,V> collection, String keyValueSeperator, String entrySeperator) {
		if(collection == null)
			return "null";
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(Entry<K,V> entr: collection.entrySet())
		{
			buffer.append(entr.getKey().toString());
			buffer.append(keyValueSeperator);
			buffer.append(entr.getValue().toString());
			if(idx != collection.size() -1)
				buffer.append(entrySeperator);
			idx++;
		}
		return buffer.toString();
	}


	public static <T> String join(T[] array, String seperator) {
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(T el: array)
		{
			buffer.append(el.toString());
			if(idx != array.length -1)
				buffer.append(seperator);
			idx++;
		}
		return buffer.toString();
	}

	public static <T> String join(T[] array, String seperator, String formattingString) {
		StringBuilder buffer = new StringBuilder();
		int idx =0;
		for(T el: array)
		{
			buffer.append(String.format(Locale.ENGLISH, formattingString, el));
			if(idx != array.length -1)
				buffer.append(seperator);
			idx++;
		}
		return buffer.toString();
	}

	public static <T> T last(List<T> collection) {
		return collection.get(collection.size() - 1);
	}

}
