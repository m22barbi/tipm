package be.uantwerpen.ldataminining.utils;

public final class AlgorithmParameters {
    private AlgorithmParameters() {}

    public static final String MINSUP = "Minsup (%)"; //ex: "(e.g. 0.5 or 50%)"  TYPE: double
    public static final String MIN_PATTERN_LENGTH = "Min pattern length"; //ex: "(e.g. 1 items)" TYPE: int
    public static final String MAX_PATTERN_LENGTH = "Max pattern length"; //ex: "(e.g. 10 items)" TYPE: int
    public static final String REQUIRED_ITEMS = "Required items"; //ex: "(e.g. 1,2,5)" TYPE: String
    public static final String MAX_GAP = "Max gap"; //ex: "(e.g. 1 item)" TYPE: int
}
