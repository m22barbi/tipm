package be.uantwerpen.ldataminining.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class MultiKeyMap2<K1,K2,V1> {

	private Map<K1, Map<K2,V1>> map = new HashMap<>();

	public void put(K1 key1, K2 key2, V1 value)
	{
		Map<K2, V1> dept1 = map.get(key1);
		if(dept1 == null) {
			dept1 = new HashMap<>();
			map.put(key1, dept1);
		}
		dept1.put(key2, value);
	}

	public V1 getOrDefault(K1 key1, K2 key2, V1 defaultValue) {
		return  map.getOrDefault(key1, new HashMap<>())
					.getOrDefault(key2, defaultValue);
	}

	public void set(K1 key1, Map<K2,V1> submap)
	{
		map.put(key1, submap);
	}


	public Map<K2, V1> get(K1 key1){
		return map.get(key1);
	}

	public V1 get(K1 key1, K2 key2){
		Map<K2, V1> dept1 = map.get(key1);
		if(dept1 == null)
			return null;
		return dept1.get(key2);
	}

	public Set<K1> keySet(){
		return map.keySet();
	}

	public Set<K2> keySet(K1 key1){
		Map<K2, V1> dept1 = map.get(key1);
		if(dept1 == null)
			return Collections.emptySet();
		else
			return dept1.keySet();
	}

	public List<Pair<K1, K2>> getKeyPairs() {
		List<Pair<K1, K2>> keyPairs = new ArrayList<>();
		for(K1 key1 : map.keySet()) {
			for(K2 key2 : map.get(key1).keySet()) {
				keyPairs.add(new Pair<>(key1, key2));
			}
		}
		return keyPairs;
	}

	public void fill(Collection<K1> keys1, Collection<K2> keys2, V1 value){
		for(K1 key1: keys1)
			for(K2 key2: keys2)
				put(key1, key2, value);
	}

	public int size(){
		int count = 0;
		for(K1 key1: map.keySet()){
			for(K2 key2: map.get(key1).keySet()){
				count++;
			}
		}
		return count;
	}

	public String toString(){
		//print something like:
		//			Key-1 ...
		//Key1		Val
		//Key2
		Set<K2> keysLevel2 = new TreeSet<>();
		for(K1 key1: keySet()){
			for(K2 key2: keySet(key1)){
				keysLevel2.add(key2);
			}
		}
		List<K2> keysLevel2Ordered = new ArrayList<>(keysLevel2);
		StringBuffer buff = new StringBuffer();
		buff.append(toString(""));
		buff.append(",");
		for(K2 key2: keysLevel2Ordered){
			buff.append(toString(key2));
			buff.append(",");
		}
		buff.append("\n");
		for(K1 key1: keySet()){
			buff.append(toString(key1));
			buff.append(",");
			for(K2 key2: keysLevel2Ordered){
				V1 val = get(key1, key2);
				buff.append(toString(val));
				buff.append(",");
			}
			buff.append("\n");
		}
		return buff.toString();
	}

	private String toString(Object o){
		String result = "";
		if(o == null){
			return String.format("%-30s","");
		}
		else{
			String v = String.valueOf(o);
			if(v.length() > 60){
				v = v.substring(0, 60) + "...";
			}
			return String.format(Locale.ENGLISH, "%-30s",v);
		}
	}

}
