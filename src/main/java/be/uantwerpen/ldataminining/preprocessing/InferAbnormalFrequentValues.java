package be.uantwerpen.ldataminining.preprocessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.MathUtils;
import be.uantwerpen.ldataminining.utils.RegexUtils;
import be.uantwerpen.ldataminining.utils.Utils;

public class InferAbnormalFrequentValues {

	/**
	 * For some colums, 0 means null, but somethimes it is a valid value, how to discriminate?
	 *
	 * Cases:
	 * Enums or No enum case:
	 * 		Value is either Date or String or Integer, but also 50% of value is '0' or 'n/a'
				-> if value if consistent in format, e.g. 0/1/2 ... -> Value is not a 'null'
																	-> Else value is a 'null'
	 * @param values
	 * @return
	 */
	public static String inferNullValueEncoding(List<String> values){
		Map<String,Integer> support = new HashMap<>();
		int isNull = 0;
		for(String value: values){
			if(CSVUtils.isEmptyValueInCSV(value)){
				isNull++;
				continue;
			}
			Integer count = support.get(value);
			count = (count == null?1:count+1);
			support.put(value, count);
		}
		int totalNotNull = values.size() - isNull;
		int totalDistinct = support.keySet().size();
		if(totalNotNull == 0)
			return null;
		if(totalDistinct < 10){
			return null;
		}
		List<Entry<String,Integer>> entriesSorted = CollectionUtils.entriesSortedByValues(support);
		String mostOccuringValue = entriesSorted.get(0).getKey();
		int mostOccuringValueSupport = entriesSorted.get(0).getValue();
		List<String> otherValues = new ArrayList<String>();
		for(int i=1; i< Math.min(50 ,entriesSorted.size()); i++){
			String otherValue = entriesSorted.get(i).getKey();
			otherValues.add(otherValue);
		}
		double supportPercentage = 100 * mostOccuringValueSupport/(double)totalNotNull;
		boolean veryFrequentValue = supportPercentage > 10.0;
		//value is int, and all others are string?
		//or value is like '[0-9]+[A-Z]+' and all others are like '[0-9]+[A-Z]+'
		boolean typeAbnormal = checkTypeAbnormal(mostOccuringValue, otherValues);
		//value is like 'aa' and all other others are 'aaaaa' 'aaaaaaa' ...
		boolean stringSizeAbnormal = checkStringSizeAbnormal(mostOccuringValue, otherValues);
		//value like 'gezondheid' and other like 'city123', 'city4343' ...
		boolean stringAbnormal = checkStringAbnormal(mostOccuringValue, otherValues);
		boolean isDecimal = isDecimal(mostOccuringValue, otherValues);
		if(!veryFrequentValue)
			return null;
		if(isDecimal){
			//value if like '100000' and others are like '5' '645' '455'
			boolean decimalAbnormal = checkDecimalAbnormal(mostOccuringValue, otherValues);
			if(decimalAbnormal){
				System.out.format(" infered as null: %s compared to %s. \n Reason: Decimal out-of-range.\n",
					mostOccuringValue, otherValues, decimalAbnormal);
				return mostOccuringValue;
			}
			else{
				return null;
			}
		}
		if(stringSizeAbnormal || typeAbnormal || stringAbnormal){
			System.out.format(" infered as null: %s compared to %s.\n  Reason,"
					+ " stringSizeAbnormal=%s, typeAbnormal=%s, stringAbnormal=%s\n",
					mostOccuringValue, otherValues, stringSizeAbnormal, typeAbnormal, stringAbnormal);
			return mostOccuringValue;
		}
		else
			return null;
	}

	/**
	 * Example 1:   [0] , [Antwerpen, Brussel ....] -> 0 is not consistent, because integer and rest is string
	 * 				'n/a', [Antwerpen, Brussel.... ] -> n/a is not consistent, because extra character '/' and size <<<
	 * @param value
	 * @param values
	 */
	public static boolean checkStringSizeAbnormal(String value, List<String> otherValues){
		double valueSize = value.length();
		List<Integer> otherLengths = new ArrayList<Integer>();
		for(String otherValue: otherValues){
			otherLengths.add(otherValue.length());
		}
		double averageSize = MathUtils.meanInt(otherLengths);
		double stdSize = MathUtils.stdInt(otherLengths);
		boolean sizeAbnormal = Math.abs(valueSize- averageSize) > 2 * stdSize;
		return sizeAbnormal;
	}

	public static boolean isDecimal(String value, List<String> otherValues){
		if(!Utils.isFloat(value))
			return false;
		for(String otherValue: otherValues)
			if(!Utils.isFloat(otherValue))
				return false;
		return true;
	}

	public static boolean checkDecimalAbnormal(String value, List<String> otherValues){
		double valueDouble = Double.parseDouble(value);
		List<Double> otherValuesDoubles = new ArrayList<Double>();
		for(String otherValue: otherValues){
			otherValuesDoubles.add(Double.parseDouble(otherValue));
		}
		double averageValue = MathUtils.mean(otherValuesDoubles);
		double stdSize = MathUtils.std(otherValuesDoubles);
		boolean abnormalValue = Math.abs(valueDouble- averageValue) > 2 * stdSize;
		return abnormalValue;
	}

	public static boolean checkTypeAbnormal(String value, List<String> otherValues){
		List<String> otherTokenTypes = new ArrayList<String>();
		for(String otherValue: otherValues){
			otherTokenTypes.add(RegexUtils.toTokenString(otherValue));
		}
		//compute Leviathian distance between all types, and compute mean, and std
		List<Integer> distances = new ArrayList<Integer>();
		for(String otherType: otherTokenTypes){
			for(String otherType2: otherTokenTypes){
				int distance = StringUtils.getLevenshteinDistance(otherType, otherType2);
				distances.add(distance);
			}
		}
		double averageDistance = MathUtils.meanInt(distances);
		double stdDistance = MathUtils.stdInt(distances);
		//compute Leviathian distance for our value
		List<Integer> myDistances = new ArrayList<Integer>();
		String tokenType = RegexUtils.toTokenString(value);
		for(String otherType: otherTokenTypes){
			int distance = StringUtils.getLevenshteinDistance(otherType, tokenType);
			myDistances.add(distance);
		}
		double myMean = MathUtils.meanInt(myDistances);
		//double myStd = MathUtils.stdInt(myDistances);
		boolean typeAbnormal = Math.abs(myMean- averageDistance) > 2 * stdDistance;
		return typeAbnormal;
	}



	public static boolean checkStringAbnormal(String value, List<String> otherValues){
		//compute Leviathian distance between all types, and compute mean, and std
		List<Integer> distances = new ArrayList<Integer>();
		for(String otherValue: otherValues){
			for(String otherValue2: otherValues){
				int distance = StringUtils.getLevenshteinDistance(otherValue, otherValue2);
				distances.add(distance);
			}
		}
		double averageDistance = MathUtils.meanInt(distances);
		double stdDistance = MathUtils.stdInt(distances);
		//compute Leviathian distance for our value
		List<Integer> myDistances = new ArrayList<Integer>();
		for(String otherValue: otherValues){
			int distance = StringUtils.getLevenshteinDistance(otherValue, value);
			myDistances.add(distance);
		}
		double myMean = MathUtils.meanInt(myDistances);
		boolean valueAbnormal = Math.abs(myMean- averageDistance) > 2 * stdDistance;
		return valueAbnormal;
	}
}
