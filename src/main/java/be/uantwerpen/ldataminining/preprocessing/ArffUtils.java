package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import be.uantwerpen.ldataminining.model.Pair;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;
import be.uantwerpen.ldataminining.utils.IOUtils.Streamer;

//TODO: Refactor, make ArffObject class...
public class ArffUtils {
	private ArffUtils() {}

	private static final Pattern COMMA_SPLIT_PATTERN = Pattern.compile("\\s*,\\s*");
	private static final Pattern WHITE_SPACE_PATTERN = Pattern.compile("\\s+");

	public enum TypeOfFeatures{
		ALL_NOMINAL,
		ALL_BINARY,
		ALL_NUMERIC,
		MIXED
	}

	public static TypeOfFeatures getTypeOfFeatures(File input) throws IOException{
		boolean allNominal = true;
		boolean allBinary = true;
		boolean allNumeric = true;
		List<String> names = getAttributeNames(input);
		List<String> types = getAttributeValues(input);
		types.remove(names.indexOf("class"));
		for(String type: types){
			if(!type.startsWith("{")){
				allNominal = false;
			}
			if(!type.equals("{0,1}") && !type.equals("{1,0}")){
				allBinary = false;
			}
			if(!type.equals("INTEGER") || !type.equals("NUMERIC")){
				allNumeric = false;
			}
		}
		if(allNominal)
			return TypeOfFeatures.ALL_NOMINAL;
		if(allBinary)
			return TypeOfFeatures.ALL_BINARY;
		if(allNumeric)
			return TypeOfFeatures.ALL_NUMERIC;
		else
			return TypeOfFeatures.MIXED;
	}

	public static long getNumberOfRows(File arffFile) throws IOException{
		final long[] count = new long[1];
		IOUtils.stream(arffFile, new Streamer() {

			@Override
			public void doLine(String line) {
				if(line.startsWith("@data")){
					count[0] = 0;
					return;
				}
				count[0]++;
			}
		});
		return count[0];
	}

	public static int getNumberOfAttributes(File file) throws IOException
	{
		Timer timer = new Timer("getNumberOfAttributes");
		List<String> lines = IOUtils.readFileUntil(file, "@data");
		int no = 0;
		for(String line: lines){
			if(line.startsWith("@attribute")) {
				no++;
			}
		}
		timer.end();
		return no;
	}

	//returns header and row
	public static Pair<List<String>,List<String>> simpleParse(File arff) throws IOException{
		List<String> lines = IOUtils.readFile(arff);
		int indexData = IOUtils.indexOf(lines, "@data");
		return new Pair<List<String>,List<String>>(lines.subList(0, indexData+1),lines.subList(indexData+1,lines.size()));
	}

	public static void updateAttributeNames(File input, List<String> newNames) throws IOException{
		List<String> types = getAttributeValues(input);
		List<String> header = makeHeader("whatever", newNames, types, false);
		File tempFile = File.createTempFile("bla","foo");
		System.out.println("tmp file created");
		IOUtils.saveFile(header, tempFile);
		System.out.println("tmp header written");
		copyData(input, tempFile);
		System.out.println("tmp data written");
		System.out.println("============================================================== " + tempFile.getAbsolutePath());
		System.out.println("============================================================== " + input.getAbsolutePath());
		input.delete();
		input.createNewFile();
		IOUtils.copy(tempFile, input);
	}

	public static void updateAttributeTypes(File input, List<String> newTypes) throws IOException{
		List<String> names = getAttributeNames(input);
		List<String> header = makeHeader("whatever", names, newTypes, false);
		File tempFile = File.createTempFile("bla","foo");
		IOUtils.saveFile(header, tempFile);
		copyData(input, tempFile);
		input.delete();
		input.createNewFile();
		IOUtils.copy(tempFile, input);
	}

	private static void copyData(File input, File outputOnlyHeader) throws IOException {
		final BufferedWriter writer = new BufferedWriter(new FileWriter(outputOnlyHeader, true));
		final GenericSerializerArff serializer = new GenericSerializerArff(input, writer);
		streamSpareOrNormalFile(input, serializer::write);
		writer.close();
	}

	public static class GenericSerializerArff{
		private boolean sparse;
		private List<String> attributes;
		private Map<String,Integer> allAtributesMap = new HashMap<>();
		private BufferedWriter writer;
		public GenericSerializerArff(File inputFile, BufferedWriter writer) throws IOException{
			this.sparse = ArffUtils.isSparse(inputFile);
			if(sparse){
				this.attributes = ArffUtils.getAttributeNames(inputFile);
				for(int i=0; i<attributes.size(); i++){
					allAtributesMap.put(attributes.get(i), i);
				}
			}
			this.writer = writer;
		}

		public void write(List<String> attributes, List<String> values){
			if(!sparse){
				try {
					String row = CSVUtils.serializeLineArff(values);
					writer.write(row);
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			else{
				try {
					List<Integer> attributesIndexes = new ArrayList<Integer>();
					for(String attribute: attributes){
						attributesIndexes.add(allAtributesMap.get(attribute));
					}
					String row = CSVUtils.serializeLineSparseArff(attributesIndexes, values);
					writer.write(row);
					writer.newLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	public static boolean isSparse(File input) {
		final Boolean[] sparse = new Boolean[]{null};
		try{
		IOUtils.stream(input, new Streamer() {

			boolean isData = false;
			@Override
			public void doLine(String line) {
				if(line.startsWith("@data"))
				{
					isData = true;
					return;
				}
				if(isData){
					if(line.isEmpty())
						return;
					if(line.startsWith("{"))
						sparse[0] = true;
					else
						sparse[0] = false;
					throw new RuntimeException("escaping streamer");
				}
			}
		});
		}catch(Exception e){
			return sparse[0];
		}
		throw new RuntimeException("should not get here");
	}


	/*
	public void transform() throws ParseException{
		String s="03FEB2012:12:39:12";
		DateFormat format = new SimpleDateFormat("ddMMMyyyy:hh:mm:ss");
		Date dateStr = format.parse(s);
		DateFormat formatISO = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
	    String formattedDate = formatISO.format(dateStr);
	    System.out.println(formattedDate);
	}
	*/


	public static void copyHeader(File file, File outputFile) throws IOException {
		List<String> header = IOUtils.readFileUntil(file, "@data");
		header.add("@data");
		header.add("");
		IOUtils.saveFile(header, outputFile);
	}

	public static String getRelationName(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file,1);
		String firstLine = lines.get(0);
		if(firstLine.startsWith("@relation")) {
			return Utils.substringAfter(firstLine, "@relation").trim();
		}
		else {
			throw new RuntimeException("@relation not found in " + file.getAbsolutePath());
		}
	}

	public static List<String> getAttributeNames(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file,"@data");
		List<String> attributeNames = new ArrayList<>();
		//fix header
		for(String line: lines){
			if(line.startsWith("@attribute")){
				String name = line.split("\\s+")[1];
				attributeNames.add(name);
			}
		}
		return attributeNames;
	}

	public static List<String> getAttributeValues(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file,"@data");
		List<String> values = new ArrayList<>();
		//fix header
		for(String line: lines){
			if(line.startsWith("@attribute")){
				String name = line.split("\\s+")[1];
				String value = Utils.substringAfter(line, name).trim();
				values.add(value);
			}
		}
		return values;
	}

	public static List<String> getClasses(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file,"@data");
		for(String line: lines){
			if(line.startsWith("@attribute class")){
				String name = line.split("\\s+")[1];
				String value = Utils.substringAfter(line, name).trim();
				value = value.substring(1, value.length()-1);
				return Arrays.asList(value.split("\\s?,\\s?"));
			}
		}
		return null;
	}

	public static List<String> filterNotNull(List<String> values) {
		List<String> notNullValues = new ArrayList<String>();
		for(String value: values){
			if(!value.equals("?")){
				notNullValues.add(value);
			}
		}
		return notNullValues;
	}

	public static List<String> getStringAttributes(File input) throws IOException{
		List<String> attributes = getAttributeNames(input);
		List<String> types = getAttributeValues(input);
		List<String> stringAttributes = new ArrayList<String>();
		for(String attribute: attributes){
			int colIdx = attributes.indexOf(attribute);
			if(!types.get(colIdx).equals("STRING"))
				continue;
			stringAttributes.add(attribute);
		}
		return stringAttributes;
	}

	public static List<String> getAttributeValues(File file, String attributeName) throws IOException{
		List<String> names = getAttributeNames(file);
		int attributeIndex = names.indexOf(attributeName);
		assert(attributeIndex != -1): "Attribute not found";
		List<String> data = IOUtils.readFile(file);
		data = data.subList(IOUtils.indexOf(data, "@data") + 1, data.size());
		boolean sparse = data.get(0).startsWith("{");
		List<String> values = new ArrayList<String>();
		if(!sparse){
			for(String line: data){
				List<String> rowValues = CSVUtils.parseLine(line, ',', '\"');
				values.add(rowValues.get(attributeIndex));
			}
		}
		else{
			for(String line: data){
				String[][] rowValues = getSparseRow(line);
				String val = null;
				for(String[] pair: rowValues){
					if(pair[0].equals(String.valueOf(attributeIndex))){
						val = pair[1];
						break;
					}
				}
				if(val != null)
					values.add(val);
			}
		}
		return values;
	}

	public static List<List<String>> loadDataMatrix(File arffFile) throws IOException
	{
		return loadDataMatrix(arffFile, -1, -1);
	}

	public static List<List<String>> loadDataMatrix(File arffFile, final int from, final int to) throws IOException
	{
		final long totalItems = IOUtils.countLines(arffFile);
		Timer timer = null;
		if(totalItems > 50000)
			timer = new Timer("loadDataMatrix " + arffFile.getName());
		final Timer timer2 = timer;
		final List<List<String>> output = new ArrayList<List<String>>();
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				if(line.isEmpty())
					return;
				if(i % 500000 == 0 && timer2 != null)
					timer2.progress("Loading",i,totalItems);
				if(!data && line.startsWith("@data")){
					data = true;
				}
				else if(data){
					i++;
					if((from ==-1 && to == -1) || (i>from && i<to))
						output.add(CSVUtils.parseLineAndUnquote(line, ',', '\"'));
				}
			}
		});
		if(totalItems > 50000)
			timer.end();
		return output;
	}

	/**
	 * Load k records
	 */
	public static List<List<String>> loadDataMatrixSample(File arffFile, int sampleSize) throws IOException
	{
		final long totalItems = numberOfRows(arffFile);
		if(sampleSize > totalItems)
			sampleSize = (int)totalItems;
		final int sampleSizefinal = sampleSize;
		Timer timer = null;
		if(totalItems > 50000)
			timer = new Timer("loadDataMatrix " + arffFile.getName());
		final Timer timer2 = timer;
		final List<List<String>> output = new ArrayList<List<String>>();
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				if(i % 500000 == 0 && timer2 !=null)
					timer2.progress("Loading",i,totalItems);
				if(!data && line.startsWith("@data")){
					data = true;
				}
				else if(data){
					i++;
					if(i % (totalItems / sampleSizefinal) == 0) //e.g.: if 100 items and sampleSize=5, will return true if i == 20, 40, 60, 80..
						output.add(CSVUtils.parseLineAndUnquote(line, ',', '\"'));
				}
			}
		});
		if(totalItems > 50000)
			timer.end();
		return output;
	}

	public static int numberOfRows(File arffFile) throws IOException{

		final int[] count = new int[]{0};
		IOUtils.stream(arffFile, new Streamer() {

			@Override
			public void doLine(String line) {
				if(line.startsWith("@data"))
					count[0]=0;
				else
					count[0]++;
			}
		});
		return count[0];
	}

	public static List<String> getColumn(List<List<String>> matrix, int colIdx){
		List<String> lst = new ArrayList<String>();
		for(List<String> row: matrix){
			lst.add(row.get(colIdx));
		}
		return lst;
	}

	public static List<String[][]> loadSparseDataMatrix(File arffFile) throws IOException
	{
		final long count = IOUtils.countLines(arffFile);
		Timer timer = null;
		if(count > 50000)
			timer = new Timer("loadSparseDataMatrix " + arffFile.getName());
		final Timer timer2 = timer;
		final List<String[][]> output = new ArrayList<String[][]>();
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				i++;
				if(i % 500000 == 0)
					timer2.progress("Loading",i,count);
				if(!data && line.startsWith("@data")){
					data = true;
				}
				else if(data)
					output.add(getSparseRow(line));
			}
		});
		if(count > 50000)
			timer.end();
		return output;
	}

	public interface StreamerArff{
		public void doSomething(String[][] row);
	}

	public interface StreamerArffRegular{
		public void doSomething(List<String> row);
	}

	public interface StreamerArffGeneric{
		public void doSomething(List<String> attributes, List<String> values); //for non-sparse: all attributes...
	}

	public static void streamSparseFile(File arffFile, final StreamerArff streamer) throws IOException
	{
		if(!isSparse(arffFile))
			throw new RuntimeException("No sparse data");
		final Timer timer = new Timer("StreamSparse " + arffFile.getName());
		final long count = IOUtils.countLines(arffFile);
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				if(!data && line.startsWith("@data")){
					i=0;
					data = true;
				}
				else if(data){
					if(line.isEmpty())
						return;
					streamer.doSomething(getSparseRow(line));
					i++;
					if(i % 500000 == 0)
						timer.progress(i,count);
				}
			}
		});
		timer.end();
	}

	public static void streamSpareOrNormalFile(File arffFile, final StreamerArffGeneric streamer) throws IOException
	{
		final Timer timer = new Timer("StreamSparse " + arffFile.getName());
		final long count = IOUtils.countLines(arffFile);
		final List<String> allAttributes = getAttributeNames(arffFile);
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;


			@Override
			public void doLine(String line) {
				if(data) {
					if(!line.startsWith("{")){
						List<String> values = CSVUtils.parseLineAndUnquote(line, ',', '\'');
						streamer.doSomething(allAttributes, values);
					}
					else{
						List<String> attributes = new ArrayList<>();
						List<String> values = new ArrayList<>();
						String[][] pairs = getSparseRow(line);
						for(String[] pair: pairs){
							String attribute = allAttributes.get(Integer.valueOf(pair[0]));
							attributes.add(attribute);
							values.add(pair[1]);
						}
						streamer.doSomething(attributes, values);
					}
					i++;
					if(i % 500000 == 0)
						timer.progress(i,count);
				} else if(line.startsWith("@data")) {
					i=0;
					data = true;
				}
			}
		});
		timer.end();
	}

	public static void streamFile(File arffFile, final StreamerArffRegular streamer) throws IOException
	{
		final Timer timer = new Timer("StreamSparse " + arffFile.getName());
		final long count = IOUtils.countLines(arffFile);
		IOUtils.stream(arffFile, new Streamer() {

			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				if(!data && line.startsWith("@data")){
					i=0;
					data = true;
				}
				else if(data){
					streamer.doSomething(CSVUtils.parseLineAndUnquote(line, ',', '\''));
					i++;
					if(i % 500000 == 0)
						timer.progress(i,count);
				}
			}
		});
		timer.end();
	}




	public static String[][] getSparseRow(String row)
	{
		row = Utils.substringBetween(row, "{","}");
		if(row.trim().isEmpty())
			return new String[0][];
		String[] pairs = COMMA_SPLIT_PATTERN.split(row);
		String[][] pairsSplitted = new String[pairs.length][];
		for(int n=0; n< pairs.length; n++)
		{
			String[] pair = WHITE_SPACE_PATTERN.split(pairs[n]);
			pairsSplitted[n] = pair;
		}
		return pairsSplitted;
	}

	public static int getClassColumnIndex(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file, "@data");
		int no = 0;
		for(String line: lines){
			if(line.startsWith("@attribute class"))
				return no + 1;
			else if(line.startsWith("@attribute"))
				no++;
		}
		throw new RuntimeException("Class attribute not found");
	}

	public static List<String> makeHeader(String relationName, List<String> attributeNames, List<String> attributeTypes, boolean escapeName){
		List<String> lines = new ArrayList<String>();
		lines.add(String.format("@relation %s", relationName));
		for(int i=0; i< attributeNames.size(); i++){
			lines.add(String.format("@attribute %s %s", escapeName?Utils.escapeName(attributeNames.get(i)):attributeNames.get(i), attributeTypes.get(i)));
		}
		lines.add("\n@data\n");
		return lines;
	}

	public static void saveFile(File output, List<String> cols, List<String> types, List<List<String>> data) throws IOException {
		List<String> header = ArffUtils.makeHeader("arff-file", cols, types, false);
		IOUtils.saveFile(header, output);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(output, true));
		for(List<String> row: data) {
			writer.write(CSVUtils.serializeLineArff(row));
			writer.newLine();
		};
		writer.close();
	}

}
