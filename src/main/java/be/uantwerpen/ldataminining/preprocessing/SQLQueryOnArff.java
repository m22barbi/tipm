package be.uantwerpen.ldataminining.preprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.h2.tools.Server;

import be.uantwerpen.ldataminining.utils.CollectionUtils;
import be.uantwerpen.ldataminining.utils.IOUtils;
import be.uantwerpen.ldataminining.utils.Timer;
import be.uantwerpen.ldataminining.utils.Utils;
/**
 * Allows queries on CSV files, using in-memory database H2
 *
 * @author lfereman
 *
 */
public class SQLQueryOnArff {

	/**
	 * Input: arffFile
	 * Output: output csv
	 * @param arffFile
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public File runQuery(File arffFile, String query) throws IOException, SQLException, ClassNotFoundException{
		System.out.println("runQuery");
		// start the TCP Server
		Server server = Server.createTcpServer().start();
		// connect
		Class.forName("org.h2.Driver");
		final Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
		final Statement stmt = conn.createStatement();

		//create table schema
		List<String> attributeNames = ArffUtils.getAttributeNames(arffFile);
		List<String> types = ArffUtils.getAttributeValues(arffFile);
		final List<String> sqlTypes = new ArrayList<>();
		for(String type: types){
			sqlTypes.add(arffToSQLType(type));
		}
		String schema = "";
		String empty_values = "";
		for(int i=0; i<attributeNames.size(); i++){
			schema += "," + attributeNames.get(i) + " " + sqlTypes.get(i);
			empty_values += ",?";
		}
		schema = schema.substring(1);
		empty_values = empty_values.substring(1);
		try{
			stmt.execute("drop table file");
			System.out.println("Drop table file");
		}catch(Exception e){

		}
		String stmt1="create table file (" + schema + ")";
		System.out.println(stmt1);
		stmt.execute(stmt1);
		//before addding data, quickly check query on empty table
		ResultSet rs = stmt.executeQuery(query);
		rs.next();
		rs.close();
		//add data
		Timer timer = new Timer("adding data");
		try{
			ArffUtils.streamFile(arffFile, (List<String> row) -> {
				List<String> sqlValues = toSQLValues(row, sqlTypes);
				try {
					String stmt2 = "insert into file values(" + CollectionUtils.join(sqlValues) + ")";
					stmt.executeUpdate(stmt2);
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			});
		}catch(Exception e){
			e.printStackTrace();
			conn.close();
			server.stop();
			return null;
		}
		timer.end();
		//run query, and get schema
		rs = stmt.executeQuery(query);
		ResultSetMetaData metadata = rs.getMetaData();
		List<String> attributeNamesResult = new ArrayList<String>();
		List<String> attributeTypesResult = new ArrayList<String>();
		int colCount = metadata.getColumnCount();
		for(int i=1; i<= colCount; i++){
			attributeNamesResult.add(metadata.getColumnLabel(i));
			attributeTypesResult.add(sqlTypeToArff(metadata.getColumnTypeName(i)));
		}

		//save result to csv
		File output = IOUtils.getFileWithDifferentExtension(IOUtils.getFileWithDifferentSuffix(arffFile, "-query"),"csv");
		Writer writer = new BufferedWriter(new FileWriter(output, false));
		writer.write(CSVUtils.serializeLineCSV(attributeNamesResult));
		writer.write("\n");
		//get data and save result set to arff
		while(rs.next()){
			List<String> values = new ArrayList<String>();
			for(int i=1; i<= colCount; i++){
				String value = rs.getString(i);
				if (rs.wasNull())
					value = "?";
				if(attributeTypesResult.get(i-1).equals("DATE")){ //do date convertion
					try {
						Date d = defaultFormatDatabase.parse(value);
						value = defaultFormatArff.format(d);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				values.add(value);
			}
			writer.write(CSVUtils.serializeLineCSV(values));
			writer.write("\n");
		}
		writer.close();
		//done
		rs.close();
		conn.close();
		// stop the TCP Server
		server.stop();
		return output;
	}

	private String sqlTypeToArff(String type) {
		if(type.equals("INT") || type.equals("INTEGER"))
			return "INTEGER";
		else if(type.equals("DOUBLE"))
			return "NUMERIC";
		else if(type.equals("BIT"))
			return "{0,1}";
		else if(type.equals("DATETIME") || type.equals("TIMESTAMP"))
			return "DATE"; //assuming ISO!
		else return "STRING";
	}

	private String arffToSQLType(String type) {
		if(type.startsWith("DATE")){
			return "DATETIME";
		}
		if(type.equals("STRING")){
			return "VARCHAR(1024)";
		}
		else if(type.equals("INTEGER")){
			return "INT";
		}
		else if(type.equals("NUMERIC") || type.equals("REAL") || type.equals("FLOAT")){
			return "DOUBLE";
		}
		else if(type.startsWith("{")){
			type = Utils.substringBetween(type, "{", "}");
			String[] values = type.split("\\s?,\\s?");
			if(values.length == 0)
				return "BIT";
			boolean allBoolean = true;
			boolean allInt = true;
			boolean allDouble = true;
			for(String value: values){
				if(!value.equals("0") && !value.equals("1"))
					allBoolean = false;
				if(!Utils.isInteger(value))
					allInt = false;
				if(!Utils.isFloat(value))
					allDouble = false;
			}
			if(allBoolean)
				return "BIT";
			if(allInt)
				return "INT";
			if(allDouble)
				return "DOUBLE";
			return "VARCHAR(1024)";
		}
		else
			return "VARCHAR(1024)";
	}

	private SimpleDateFormat defaultFormatDatabase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //changed and dropped ss
	private SimpleDateFormat defaultFormatArff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private List<String> toSQLValues(List<String> row, List<String> sqlTypes) {
		List<String> newValues = new ArrayList<String>();
		for(int i=0; i< sqlTypes.size(); i++){
			if(row.get(i).equals("?"))
				newValues.add("NULL");
			else if(sqlTypes.get(i).equals("INTEGER") || sqlTypes.get(i).equals("INT") || sqlTypes.get(i).equals("DOUBLE")){
				newValues.add(row.get(i));
			}
			else if(sqlTypes.get(i).equals("DATETIME") || sqlTypes.get(i).equals("TIMESTAMP")){
				try {
					Date d = defaultFormatArff.parse(row.get(i));
					String value = "'" + defaultFormatDatabase.format(d) + "'";
					newValues.add(value);
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
			}
			else{
				newValues.add("'" + row.get(i) + "'");
			}
		}
		return newValues;
	}
}
