<html>
<body>
<%@include file="header.jsp" %>

<script>
//show combobox with selectable projects
function show_projects(id, only_selectable=false){
 $.ajax({ url: "/rest/projects", context: document.body }).done(function(projects) {
 			$(id).empty(); //clear comboboxes 
 			projects.sort();
  			for(var i=0;i<projects.length;i++){
  				if(only_selectable && projects[i]['fileItems'].length == 0){
  					continue;
  				}
  				else{
  					$(id).append('<option value=' +projects[i]['name'] + '>' + projects[i]['name'] + '</option>');
  				}
  			}
  			//set current project, based on active session
  			$(id).val('${sessionScope.mySession.currentProject}');
		}); 
}

//add new selectable projects
function add_project(project_name){
	url = '/rest/project/create';
 	$.post( url, {name : project_name }).done(function(data ) {
   		 $( "#create_project_status" ).empty().append("Done");
   		 show_projects('#project0', true);
  		 show_projects('#project');
  	});
}

//setActive project
function select_item(project, itemId)
{
   var url = "/rest/item/set-current?project=" + encodeURIComponent(project) + "&item=" + encodeURIComponent(itemId);
   $.ajax({ url: url}).done(function(data) {
   		window.location.href = "/transform";
   	});
}

//remove dataset
function remove_item(itemId)
{
  var url = '/rest/item/remove?item=' + encodeURIComponent(itemId);
  var status_element_id = "#remove_file_status";
  do_basic_post_with_reporting_call(
  		url, status_element_id, 
  		function(data){
  			//rest service selects previous version of file, in back end
  			window.location.href = "/list";
  		}
  );
}

//show datasets
function update_projects()
{
  $.ajax({ url: "/rest/projects", context: document.body }).done(
    function(projects) {
    $('#projects').find("tr:gt(0)").remove();
    for(var i=0;i<projects.length;i++){
        var project = projects[i];
        var name = project['name'];
        var current_project = $("#project0 option:selected").text();
        var current_item = '${sessionScope.mySession.currentItem}'; //from session
        if(name !== current_project && current_project.length > 0){
        		continue;
        }
        var last_logical_name = 'None';
        var odd = false;
        for(var j=0;j<project['fileItems'].length;j++){
          var id = project['fileItems'][j]['id'];
          var logical_name = project['fileItems'][j]['logicalName'];
          var version = project['fileItems'][j]['version'];
          var stackOperationsAll = project['fileItems'][j]['stackOperations'].join(' &#013;');
          stackOperationsAll = replaceAll(stackOperationsAll, '\\(', ' (');
          stackOperationsAll = replaceAll(stackOperationsAll, '\\,', ', ');
          var stackOperationsShorter = '';
          for(var k=project['fileItems'][j]['stackOperations'].length-1; k>=0; k--){
          	  var current =  project['fileItems'][j]['stackOperations'][k];
          	  current = replaceAll(current, '\\(', ' (');
              current = replaceAll(current, '\\,', ', ');
          	  stackOperationsShorter = stackOperationsShorter + current + '<br/>';
          	  break;
          }
          if(project['fileItems'][j]['stackOperations'].length > 1){
          	  stackOperationsShorter = stackOperationsShorter + '...' + (project['fileItems'][j]['stackOperations'].length -1) + ' more';
          }
          var noRows =  project['fileItems'][j]['noRows'];
          var noColumns = project['fileItems'][j]['noColumns'];
          var style_row = "style=\"background-color: rgba(245,245,245, 0.6)\"";
          var style_cell = ""
      	  if(last_logical_name != logical_name){
          	odd = !odd;
            if(j!=0){
            		style_cell = " style=\"border-top: thin solid;\" "
            }
          }
          if(!odd){
          	style_row = "style=\"background-color: #ffffff\"";
          }
          var selected_style = '';
          if(id == current_item){
          		selected_style= " style=\"background-color: rgb(255,255,0)\" ";
          } 
          $('#projects').append('<tr ' + style_row + '><td' + style_cell + '>' + name + '</td>' + 
                        '<td' + style_cell + '><a ' + selected_style + ' href="javascript:select_item(\'' + name + '\',\'' +  id + '\')">' +  logical_name + '</a></td>' + 
                        '<td' + style_cell + '>' + version + '</td>' + 
                        '<td' + style_cell + ' style="word-wrap: break-word" title="' + stackOperationsAll + '">' + stackOperationsShorter + '</td>' +
                        '<td' + style_cell + '>' + noColumns + '</td>' + 
                        '<td' + style_cell + '>' + noRows + '</td>' +
                        '<td' + style_cell + '><a href="javascript:remove_item(\'' + id + '\')"><img src="images/remove_icon.png" style=\"width:15px\"></img></a></td></tr>'
                        );
          last_logical_name = logical_name;
        } //end loop datasets within project
      } //end loop project
    });
}

 
$( document ).ready(function() {
  show_projects('#project0', true);
  show_projects('#project');
  update_projects();   
  $("#add_project" ).submit(function( event ) {
  		event.preventDefault();
 		var project_name=$("#pname").val();
 		add_project(project_name);
  });	
  $("input:file").change(function (){
       var fileName = $(this).val();
       fileName = fileName.replace(/C:\\fakepath\\/i, '');
       $("#name").val(fileName);
  });
  $("#add_project" ).submit(function( event ) {
  		event.preventDefault();
 		var project_name=$("#pname").val();
 		add_project(project_name);
  });
  $("#project0" ).change(function() {
  		event.preventDefault();
 		update_projects();
  });
});


</script>
<div id="container">
   <div id="content"  style="padding: 10px;">
  	
  <table> 
   			<tr>
   			<!-- left: list of datasets per project -->
   			<td valign="top">
   			
   <div id="#remove_file_status"/>
   <table id="projects" class="table" cellspacing="0" 
   			style="font-size: 1.1em; table-layout: 100%; margin: 5px">
   	  <thead><tr>
   	  		<th style="width:150px"><select name="project0" id="project0">
   				</select></th>
   			<th>Dataset</th><th>Version</th><th>Actions</td><th>Columns</th><th>Rows</th><th></th></tr></thead>
   </table>

			</td>
			<!-- right: upload form -->
			<td valign="top">
				
  		<form method="POST" enctype="multipart/form-data" action="/rest/upload" >	
		<table class="actions" style="width:100%;">
			<tr><td style="width:100px;">File</td>   
				<td><input type="file" name="file"></td></tr>
			<tr><td>Name</td>     
				<td><input class="field" type="text" name="name" id="name" placeholder="Dataset name"></td></tr>
			<tr><td>Project</td>          
				<td>
					<select class="combo" name="project" id="project" placeholder="Select project"></select>
					&nbsp;
					<input class="button" type="submit" value="Upload"/>
				 </td> 
			</tr>
		</table>
		</form>
		<form id="add_project" method="POST" enctype="multipart/form-data" action="/" >
		<table class="actions" style="width:100%;">
			<tr><td style="width:100px;">New project</td>
				<td>
					<input class="field" type="text" name="pname" id="pname" placeholder="Project name"></input> 
					<input class="button" type="submit" value="Add"/> 
					<span id="create_project_status"/>
				</td>
			</tr>	
		</table>
		</form>

		 </td>
		 </tr></table>

   </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>