<html>
<body>
<%@include file="header.jsp" %>
<script>

function page_refresh(data){
  window.location.href = "/mine";
}

function refresh(data){
  function show_last(){
  	var idx = 0;
  	if(pattern_metadata.length > 0){
  	   idx = pattern_metadata.length-1;
  	}
  	show_patterns_table(idx);
  }
  init_patterns_table(show_last);
  show_patternset_selections("#selected_patternsets", "chosenPatternsets");
  show_patternset_selections("#selected_patternsets2", "chosenPatternsets2");
}

function refresh2(data){
  function show_current(){
  	  show_patterns_table(currentPatternSetIdx);
  }
  init_patterns_table(show_current);
  show_patternset_selections("#selected_patternsets", "chosenPatternsets");
  show_patternset_selections("#selected_patternsets2", "chosenPatternsets2");
}

function run_itemsets(columns,algorithm, support){
  var url = '/rest/mining/run-itemsets';
  var status_element_id = "#run_itemsets_status";
  do_basic_post_with_reporting_call(url,status_element_id,{columns:columns,algorithm: algorithm, support:support},refresh);
}

function run_sp(columns,algorithm, support){
  var url = '/rest/mining/run-sp';
  var status_element_id = "#run_sp_status";
  do_basic_post_with_reporting_call(url,status_element_id,{columns:columns,algorithm: algorithm, support:support},refresh);
}

function run_filter_length(filename, minlen, maxlen){
  var url = '/rest/mining/filter-length';
  var status_element_id = "#filter_patterns_length_status";
  do_basic_post_with_reporting_call(url,status_element_id,{filename:filename, minlen:minlen, maxlen:maxlen}, refresh2);
}

function run_filter_support(filename, topk){
  var url = '/rest/mining/filter-support';
  var status_element_id = "#filter_patterns_support_status";
  do_basic_post_with_reporting_call(url,status_element_id,{filename:filename, topk:topk}, refresh2);
}

function run_remove_redundant(filename, threshold){
  var url = '/rest/mining/filter-jaccard';
  var status_element_id = "#remove_jaccard_status";
  do_basic_post_with_reporting_call(url,status_element_id,{filename:filename, threshold:threshold}, refresh2);
}

function run_filter_time_constraints(filename, maxgap, maxspan){
  var url = '/rest/mining/filter-time-constraint';
  var status_element_id = "#filter_occ_time_status";
  do_basic_post_with_reporting_call(url,status_element_id,{filename:filename, maxgap:maxgap, maxspan:maxspan}, refresh2);
}

function run_fpof(filenames){
  var url = '/rest/mining/anomaly-detection-fpof';
  var status_element_id = "#fpof_status";
  do_basic_post_with_reporting_call(url,status_element_id,{'patternFilenames':filenames.join()}, refresh2);
}

function run_pbad(filenames){
  var url = '/rest/mining/anomaly-detection-pbad';
  var status_element_id = "#pbad_status";
  do_basic_post_with_reporting_call(url,status_element_id,{'patternFilenames':filenames.join()}, refresh2);
}

function remove_pattern_set(idx)
{
  var filename = pattern_metadata[idx]['filename'];
  var url = '/rest/mining/remove-patternset?filename=' + encodeURIComponent(filename);
  var status_element_id = "#patterns_status";
  do_basic_post_with_reporting_call(url, status_element_id, {}, refresh);
}

function show_column_selections(id,clazz){
  $.ajax({ url: "/rest/metadata/attributes", async: false, context: document.body }).done(function(data) {
       for (var i = 0; i < data.length; i++ ) {
           var column = Object.keys(data[i])[0];
           $(id).append($("<option></option>").attr("value",column).text(column));
       }
       $("." + clazz).chosen();
       $("." + clazz).css({"width": "150px"});
       $("." + clazz).next().css('font-size','11px');
  });
}

function show_patternset_selections(id,clazz){
	$.ajax({ url: "rest/metadata/fileitem"}).done(function(data) {
		$(id).empty();
		var pattern_metadata = data["patterns"];
	    for(var i=0;i<pattern_metadata.length;i++){
	        var filename = pattern_metadata[i]['filename'];
	        var label = pattern_metadata[i]['label'];
	        $(id).append($("<option></option>").attr("value",filename).text(label));
	    }
        $("." + clazz).chosen();
        $("." + clazz).css({"width": "150px"});
        $("." + clazz).trigger('chosen:updated');
        $("." + clazz).next().css('font-size','11px');
    });
}

//show overview of available pattern sets
var pattern_metadata = NaN;
function init_patterns_table(after_function)
{
	$.ajax({ url: "rest/metadata/fileitem"}).done(function(data) {
		pattern_metadata = data["patterns"];
		$('#patternsets').find("tr:gt(0)").remove();
	    for(var i=0;i<pattern_metadata.length;i++){
	        var patternset = pattern_metadata[i];
	        //e.g. "patterns":[{"filename":"ambient_temp-007.arff_Charm_bitset_patterns.csv","type":"itemsets","columns":"value","algorithm":"Charm_bitset","support":"10","noPatterns":283}]
	        var label = patternset['label'];
	        var name = patternset['filename'];
	        var type = patternset['type'];
	        var columns = patternset['columns'];
	        var algorithm = patternset['algorithm'];
	        var support = patternset['support'];
	        var noPatterns = patternset['noPatterns'];
	        $('#patternsets').append('<tr>' +
	        					'<td><a class="selectlink" id="selectlink_' + i + '" href="javascript:show_patterns_table(\'' + i + '\')">' + label +'</a></td>' +
	        					'<td>' + type + '</td>' +
	                        '<td>' + columns + '</td>' +
	                        '<td> Algorithm:' + algorithm + ' Support:' + support + '</td>' +
	                        '<td>' + noPatterns + '</td>' +
	                        '<td><a href="javascript:remove_pattern_set(\'' + i + '\')"><img src="images/remove_icon.png" style=\"width:15px\"></img></a></td></tr>'
	                        );
	    }
	    after_function(); //call
	}).fail(function(data){
        show_error("#patterns_status");
    });
}


//show patterns
var showK = 10;
var rows = '';
var simplify = '';
var currentPatternSetIdx = '';
var descending = true;
var sort_last_columnId = 1;


function show_patterns_table(idx)
{
   currentPatternSetIdx = idx;
   //show selected pattern set highlighted in overview of pattern sets:
   $('a.selectlink').each(function() {
      $(this).css({ 'background-color' : '', 'opacity' : '' });
   });
   $('#selectlink_' + idx).css('background-color', 'rgb(255,255,0)');
   //load and show data
   var filename = pattern_metadata[idx]['filename'];
   show_start_loading("#patterns_status");
   $.ajax({ url: "rest/load-patterns-metadata?filename=" + encodeURIComponent(filename) }).done(function(data) {
         show_stop_loading("#patterns_status");
         $('.a')
       	 $('#patterns').find("tr:gt(0)").remove();
         rows = data['rows'];
         rows = rows.slice(1);
         //if only one time series as input: do not show label, e.g. show 'value=7 value=9' as '7 9'
         simplify = pattern_metadata[idx]['columns'] + '=';
         if(pattern_metadata[idx]['columns'].indexOf(',') != -1){
         	simplify = '';
         }
         for(var i=0;i<rows.length && i<showK;i++){
	        $('#patterns').append(patternRow2html(rows[i]));
	      }
	      $('#patterns_more').empty();
	      var html = '';
	      for(var i=0;i<rows.length && i < showK * 10;i+=showK){
	      	  html += '<a href="javascript:show('+ i+ ')">' + Math.floor(i/showK) + '</a> '
	      }
	      if(rows.length >  showK * 10){
	          var last_i =  Math.floor(rows.length / showK)-1; //-1 ok?
	      	  html += '... <a href="javascript:show('+ last_i * showK+ ')">' + last_i + '</a> '
	      }
	      if(rows.length <= showK){
	      	 html = '';
	      }
	      var remaining = Math.max(rows.length-showK,0);
	      $('#patterns_more').html('<span>' + remaining + ' more patterns.</span>&nbsp;&nbsp;&nbsp;&nbsp;' + '<div style="float:right">' + html + '</div>');
    }).fail(function(data){
        show_error("#patterns_status");
    });
}


function sortPatterns(columnId){
	if(columnId == sort_last_columnId){
		descending = !descending;
	}
	sort_last_columnId = columnId;
	function compare(a,b){
		var val1 =  a[columnId];
		var val2 =  b[columnId];
		if(isFloat(val1) && isFloat(val2)){
			val1 = parseFloat(val1);
			val2 = parseFloat(val2);
		}
		var score = (val1 > val2) ? 1 : ((val1 < val2) ? -1 : 0);
		if(descending){
			score = -score;
		}
		return score;
	}
	rows.sort(compare);
	show(0);
}

function patternRow2html(row){
	 var pattern = row[0];
	 pattern = replaceAll(pattern, simplify, '');
  	 pattern = pretty_print_pattern(pattern);
     var support = row[1];
     var conf = parseFloat(row[2]);
     var windows_all = row[3];
     var windows_abnormal = row[4];
     return '<tr><td>' + pattern + '</td><td>' + support + '</td><td>' + format(conf) + '</td><td>' + format(1.0 - conf) + '</td></tr>';
}

function show(offset){
	$('#patterns').find("tr:gt(0)").remove();
	for(var i=offset;i<rows.length && i<offset+showK;i++){
	    //var pattern = rows[i][0];
	    //pattern = replaceAll(pattern, simplify, '');
	    //var support = rows[i][1];
	    //$('#patterns').append('<tr><td>' + pattern + '</td><td>' + support + '</td></tr>');
		$('#patterns').append(patternRow2html(rows[i]));
	}
}


$( document ).ready(function() {
  show_column_selections("#selected_columnsIT","chosenIT");
  show_column_selections("#selected_columnsSP","chosenSP");
  show_patternset_selections("#selected_patternsets", "chosenPatternsets");
  show_patternset_selections("#selected_patternsets2", "chosenPatternsets2");
  $(".chosen3").chosen();
  $(".chosen3").next().css('font-size','11px');
  function show_first(){
     if(pattern_metadata && pattern_metadata.length > 0){
        show_patterns_table(0);
     }
  }
  init_patterns_table(show_first);

  $("#run_itemsets" ).submit(function( event ) {
      event.preventDefault();
      var columns=$("#selected_columnsIT").val().join();
      var support=$("#supportIT").val();
      var algo=$("#algorithmIT").val();
      run_itemsets(columns,algo, support);
  });
  $("#run_sp" ).submit(function( event ) {
      event.preventDefault();
      var columns=$("#selected_columnsSP").val().join();
      var support=$("#supportSP").val();
      var algo=$("#algorithmSP").val();
      run_sp(columns,algo, support);
  });
  $("#filter_patterns_length" ).submit(function( event ) {
      event.preventDefault();
      var filename = pattern_metadata[currentPatternSetIdx]['filename'];
      var minlen=$("#minlen").val();
      var maxlen=$("#maxlen").val();
      run_filter_length(filename, minlen, maxlen);
  });
  $("#filter_patterns_support" ).submit(function( event ) {
      event.preventDefault();
      var filename = pattern_metadata[currentPatternSetIdx]['filename'];
      var topk=$("#topk").val();
      run_filter_support(filename, topk);
  });
  $("#remove_jaccard" ).submit(function( event ) {
      event.preventDefault();
      var filename = pattern_metadata[currentPatternSetIdx]['filename'];
      var threshold=$("#jaccard").val();
      run_remove_redundant(filename, threshold);
  });
  $("#filter_occ_time" ).submit(function( event ) {
      event.preventDefault();
      var filename = pattern_metadata[currentPatternSetIdx]['filename'];
      var maxgap=$("#maxgap").val();
      var maxspan=$("#maxspan").val();
      run_filter_time_constraints(filename, maxgap, maxspan);
  });
  $("#run_fpof" ).submit(function( event ) {
      event.preventDefault();
      var filenames=$("#selected_patternsets").val();
      run_fpof(filenames);
  });
  $("#run_pbad" ).submit(function( event ) {
      event.preventDefault();
      var filenames=$("#selected_patternsets2").val();
      run_pbad(filenames);
  });


});

</script>
   <div id="content">
   <div id="container">

   	  <!-- table for layout -->
   	  <table>
   	  <tr>
   	  <!-- left: patternsets and patterns -->
   	  <td valign="top">
	   	  <table id="patternsets" class="table" cellspacing="0"  style="font-size: 1.1em; table-layout: 100%; padding: 5px; width:100%; word-wrap:break-word; table-layout: fixed;">
	   	  	<tr><th/><th>Type</th><th>Input</th><th>Parameters</th><th>No Patterns</th><th></th></tr>
	   	  </table>

	   	  <table id="patterns" class="table" cellspacing="0" style="font-size: 1.1em;table-layout: 100%; padding: 5px;  width:100%; height:350px; overflow:auto;">
	   	  		<tr>
	   	  			<th style="width:85%"><a style="color:#ffffff" onclick="sortPatterns(0)">Patterns</a></th>
	   	  			<th><a style="color:#ffffff" onclick="sortPatterns(1)">Support</a></th>
	   	  			<th><a style="color:#ffffff" onclick="sortPatterns(2)">Conf. norm.</a></th>
	   	  			<th><a style="color:#ffffff" onclick="sortPatterns(2)">Conf. abnorm.</th>
	   	  		</tr>
	   	  </table>
	   	  <div style="font-family: Consolas, monaco, monospace; font-size:0.9em; padding-left:5px" id="patterns_more">
	   	  </div>
	   	  <br/>
	   	  <div id="patterns_status"></div>
	  </td>
	  <!-- right: settings for mining -->
	  <td valign="top">

	  	  <!-- mine itemsets and sequential patterns -->

	   	  <table class="actions" style="table-layout: fixed; width: 510px">
	      <tr><td style="width: 150px" valign="top">Mine itemsets</td>
	          <td valign="top" style="width: 350px">
	            	   <form id="run_itemsets" enctype="multipart/form-data" method="POST" action="/" >
	            	   <select name="selected_columnsIT" id="selected_columnsIT" multiple="true" class="chosenIT"
	                       style="width: 200px; padding: 5px;"
	               		   data-placeholder="Select more columns for multi-dimensional mining">
	               </select>
	            	   <select name="algorithmIT" id="algorithmIT" class="chosen3" style="width: 200px">
	            	   		<option value="Eclat">Mining frequent itemsets using Eclat</option>
	            	   		<option value="Charm_bitset">Mining frequent closed itemsets using Charm</option>
	            	   		<option value="Charm_MFI">Mining frequent maximal itemsets using Charm-MFI</option>
	            	   		<option value="AprioriRare">Mine minimal rare itemsets using AprioriRare</option>
	       		   </select>
	            	   <input class="field" id="supportIT" type="text" name="support" style="width:100px" placeholder="Support"></input>
	               <input class="button" type="submit" value="Run"/>
	              </form>
	              <span id="run_itemsets_status"/>
	          </td>
	      </tr>
	      <tr><td valign="top">Mine sequential patterns</td>
	            <td valign="top">
	            		<form id="run_sp" enctype="multipart/form-data" method="POST" action="/" >
	            	   <select name="selected_columnsSP" id="selected_columnsSP" multiple="true" class="chosenSP"
	                       style="width: 200px; padding: 5px;"
	               		   data-placeholder="Select more columns for multi-dimensional mining">
	               </select>
	            	   <select name="algorithmSP" id="algorithmSP" class="chosen3" style="width: 200px">
	            	   		<option value="PrefixSpan">Mining frequent sequential patterns using PrefixSpan</option>
	            	   		<option value="CM-ClaSP">Mining frequent closed sequential patterns using CM-ClaSP</option>
	            	   		<!-- <option value="MaxSP">Mining frequent maximal sequential patterns using MaxSP</option> -->
	            	   		<option value="VMSP">Mining frequent maximal sequential patterns using VMSP</option>
	       		   </select>

	            	   <input class="field" id="supportSP" type="text" name="support" style="width:100px" placeholder="Support"></input>
	               <input class="button" type="submit" value="Run"/>
	               </form>
	              <span id="run_sp_status"/></td>
	      </tr>
	    	  </table>

	  	  <!-- filter options -->
	   	  <table class="actions" style="table-layout: fixed; width: 500px">
	   	  <tr><td style="width: 150px" valign="top">Filter patterns on length</td>
	           <td valign="top" style="width: 350px">
	            	   <form id="filter_patterns_length" enctype="multipart/form-data" method="POST" action="/" >
	            	   <input class="field" type="text" id="minlen" name="minlen" placeholder="Min length" style="width:100px"/> -
	            	   <input class="field" type="text" id="maxlen" name="maxlen" placeholder="Max length" style="width:100px"/>
	               <input class="button" type="submit" value="Run"/>
	               </form>
	               <span id="filter_patterns_length_status"/>
	            </td>
	      </tr>
	      <tr><td valign="top">Filter patterns on support</td>
	            <td valign="top">
	            <form id="filter_patterns_support" enctype="multipart/form-data" method="POST" action="/" >
	            <input class="field" type="text" id="topk" name="topk" placeholder="Top-k" style="width:80px"/>
	            <input class="button" type="submit" value="Run"/>
	            </form>
	            <span id="filter_patterns_support_status"/>
	            </td>
	      </tr>
	      <tr><td valign="top">Remove redundant patterns</td>
	            <td valign="top">
	            <form id="remove_jaccard" enctype="multipart/form-data" method="POST" action="/" >
	            If similarity &gt; <input class="field" type="text" id="jaccard" name="jaccard" placeholder="Threshold" style="width:110px"/>
	            <input class="button" type="submit" value="Run"/>
	            </form>
	            <span id="remove_jaccard_status"/>
	            </td>
	      </tr>
	   	  <tr><td valign="top">Filter on time constraints</td>
	          <td valign="top">
	            <form id="filter_occ_time" enctype="multipart/form-data" method="POST" action="/" >
	              If gap&nbsp; &gt; <input class="field" type="text" id="maxgap" name="maxgap" placeholder="Max gap" style="width:90px"/>
	              or span &gt; <input class="field" type="text" id="maxspan" name="maxspan" placeholder="Max span" style="width:90px"/>
	              <input class="button" type="submit" value="Run"/>
	            </form>
	            <span id="filter_occ_time_status"/>
	          </td>
	      </tr>
	      </table>

	      <!-- anomaly  options -->
	      <table class="actions" style="table-layout: fixed; width: 500px">
	   	  <tr><td style="width: 150px" valign="top">FP outlier factor</td>
	          <td valign="top" style="width: 350px">
	            <form id="run_fpof" enctype="multipart/form-data" method="POST" action="/" >
	            <select name="selected_patternsets" id="selected_patternsets" multiple="true" class="chosenPatternsets"
	                       style="width: 200px; padding: 5px;"
	               		   data-placeholder="Select one or more pattern sets">
	            </select>
	            <input class="button" type="submit" value="Run"/>
	            </form>
	             <span id="fpof_status"/>
	           </td>
	      </tr>
	      <tr><td valign="top">Pattern based anomaly detection</td>
	          <td valign="top">
	           <form id="run_pbad" enctype="multipart/form-data" method="POST" action="/" >
	           <select name="selected_patternsets2" id="selected_patternsets2" multiple="true" class="chosenPatternsets2"
	                       style="width: 200px; padding: 5px;"
	               		   data-placeholder="Select one or more pattern sets">
	            </select>
	           <input class="button" type="submit" value="Run"/>
	           </form>
	           <span id="pbad_status"/>
	          </td>
	      </tr>
	      </table>

	  <!-- end layout -->
    	  </td>
    	  </table>

   </div>
   </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
